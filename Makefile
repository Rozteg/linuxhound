CC=g++
SRC=src/
CFLAGS=-ggdb -std=c++17
# Comment ARMA_USE_WRAPPER in /usr/include/armadillo_bits/config.hpp
# also define there ARMA_USE_LAPACK and ARMA_USE_BLAS
# else you will be unable to use matrix operations
LDFLAGS=-Wl,-rpath /usr/local/lib/ -lbb_api -lftd2xx -lpthread -lfftw3 -lfftw3f \
-llapack -lblas
SOURCES=main.cpp $(shell find $(SRC) -name '*.cpp')
INCLUDES=./inc/
OBJECTS=$(SOURCES:.cpp=.o)
EXE=mobnr

all: $(EXE)

$(EXE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -I$(INCLUDES) -o $@

.cpp.o:
	$(CC) $(CFLAGS) -I$(INCLUDES) $< -c -o $@

link:
	ln -s ./configs/Config.xml ./Config.xml

clean:
	rm -f *~ $(EXE)
	rm -f *.o
	rm -f src/*.o
