#include "UDPServer.h"

using namespace std;

int UDPServer::sockfd;
struct sockaddr_in UDPServer::servaddr;
bool UDPServer::isSet = false;

int UDPServer::Setup(string addr, int port)
{
    UDPServer::sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(UDPServer::sockfd < 0)
    {
        cerr << "cannot open socket" << endl;
        return -1;
    }
    
    bzero(&UDPServer::servaddr,sizeof(UDPServer::servaddr));
    UDPServer::servaddr.sin_family = AF_INET;
    UDPServer::servaddr.sin_addr.s_addr = inet_addr(addr.c_str());
    UDPServer::servaddr.sin_port = htons(port);

	UDPServer::isSet = true;    

    return 0;
}

int UDPServer::Send(string msg)
{
	int bytesSent = sendto(UDPServer::sockfd, msg.c_str(), strlen(msg.c_str()), 0,
               (sockaddr*)&UDPServer::servaddr, sizeof(UDPServer::servaddr));

    if (bytesSent < 0)
    {
        cerr << "cannot send message" << endl;
        return -1;
    }

    return bytesSent;
}

void UDPServer::Close()
{
	if(UDPServer::isSet)
	{
		close(UDPServer::sockfd);
		UDPServer::isSet = false;
	}
}


