#include "AntennaSwitch.hpp"

using namespace std;

const int AntennaSwitch::ANTENNAS_QUANTITY = ANTENNAS;
const string AntennaSwitch::SERIAL_NUM = "A9003E9w"; // "A9003E9v"
const string AntennaSwitch::DESCRIPTION = "FT245R USB FIFO"; // "USB <-> Serial"; //"FT232B-FT245B"; // "FT232R"

FT_STATUS AntennaSwitch::ftStatus = FT_OK;
FT_HANDLE AntennaSwitch::ftdiDevice = NULL;

const AntennaSwitch::Antenna AntennaSwitch::initialAntenna = AntennaSwitch::Antenna::ONE;

void AntennaSwitch::OpenDevice()
{	
	CloseDevice();

	DWORD numDevs = 0;
	ftStatus = FT_CreateDeviceInfoList(&numDevs);
	FT_DEVICE_LIST_INFO_NODE ftdiDeviceList[(int)numDevs];

	ftStatus = FT_GetDeviceInfoList(ftdiDeviceList ,&numDevs);

	cout << "NumDevs:" << numDevs << endl;

	for (int i = 0; i < (int)numDevs; i++)
	{
		cout 	<< "Device Index: " << i << "\n"
				<< "Flags:" << ftdiDeviceList[i].Flags << "\n"
				<< "Location ID: " << ftdiDeviceList[i].LocId << "\n"
				<< "Description: " << ftdiDeviceList[i].Description << "\n"
				<< "ID: " << ftdiDeviceList[i].ID << "\n"
				<< "Serial: " << ftdiDeviceList[i].SerialNumber << "\n"
				<< "Type: " << ftdiDeviceList[i].Type << "\n";
	}

	if (FILE *file = fopen("./SERIAL.txt", "r"))
	{
		fclose(file);
		ifstream t("./SERIAL.txt");
		stringstream buffer;
		buffer << t.rdbuf();
		const char* c = buffer.str().c_str();
		ftStatus = FT_OpenEx((void*)c, FT_OPEN_BY_SERIAL_NUMBER, &ftdiDevice);
	}
	else if (FILE *file = fopen("./DESCRIPTION.txt", "r"))
	{
		fclose(file);
		ifstream t("./DESCRIPTION.txt");
		stringstream buffer;
		buffer << t.rdbuf();
		const char* c = buffer.str().c_str();
		ftStatus = FT_OpenEx((void*)c, FT_OPEN_BY_DESCRIPTION, &ftdiDevice);
	}
	else
	{
		const char* c = DESCRIPTION.c_str();
		ftStatus = FT_OpenEx((void*)c, FT_OPEN_BY_DESCRIPTION, &ftdiDevice);
		//ftStatus = FT_Open(0, &ftdiDevice);
		//ftStatus = FT_OpenEx(SERIAL.c_str(), FT_OPEN_BY_SERIAL_NUMBER, &ftdiDevice);
	}

	if (ftStatus != FT_OK)
		throw runtime_error("Failed to open FTDI Device! " + to_string(static_cast<int>(ftStatus)));

	ftStatus = FT_SetBaudRate(ftdiDevice, 256000); // 62500 50-60ms are ok // 256000: 5ms and 10ms are OK  // 234076

	UCHAR mask = 0x07;
	UCHAR mode = 1; // Async bitbang
	ftStatus = FT_SetBitMode(ftdiDevice, mask, mode);

	if (ftStatus != FT_OK)
		throw runtime_error("Failed to open FTDI Device! " + to_string(static_cast<int>(ftStatus)));
}

AntennaSwitch::Antenna AntennaSwitch::GetCurrentAntenna()
{
	UCHAR bitMode;
	ftStatus = FT_GetBitMode(ftdiDevice, &bitMode);
	bitMode &= 0x07;
	return static_cast<Antenna>((int)bitMode);
}



void AntennaSwitch::SwitchAntenna(Antenna antenna)
{
	UCHAR buf = (unsigned char)antenna;
	DWORD written = 0;
	Antenna currentAntenna;
	ftStatus = FT_Write(ftdiDevice, &buf, sizeof(buf), &written);
	usleep(5);
	currentAntenna = GetCurrentAntenna();
	if ((ftStatus != FT_OK) || (antenna != currentAntenna))
		throw runtime_error("Failed to switch Antenna! " + to_string(static_cast<int>(ftStatus)));
}

AntennaSwitch::Antenna AntennaSwitch::GetNextAntenna(AntennaSwitch::Antenna antenna)
{
	switch (antenna)
	{
		case ONE:
			antenna = TWO;
			break;
		case TWO:
			antenna = THREE;
			break;
		case THREE:
			antenna = FOUR;
			break;
		case FOUR:
			antenna = FIVE;
			break;
		case FIVE:
			antenna = SIX;
			break;
		case SIX:
			antenna = SEVEN;
			break;
		case SEVEN:
			antenna = EIGHT;
			break;
		case EIGHT:
			antenna = ONE;
			break;
		default:
			break;
	}
	return antenna;
}

void AntennaSwitch::NextAntenna()
{
	Antenna antenna = GetNextAntenna(GetCurrentAntenna());
	SwitchAntenna(antenna);
}

AntennaSwitch::Antenna AntennaSwitch::GetPrevAntenna(Antenna antenna)
{
	switch (antenna)
	{
		case EIGHT:
			antenna = SEVEN;
			break;
		case SEVEN:
			antenna = SIX;
			break;
		case SIX:
			antenna = FIVE;
			break;
		case FIVE:
			antenna = FOUR;
			break;
		case FOUR:
			antenna = THREE;
			break;
		case THREE:
			antenna = TWO;
			break;
		case TWO:
			antenna = ONE;
			break;
		case ONE:
			antenna = EIGHT;
			break;
		default:
			break;
	}
	return antenna;
}

void AntennaSwitch::PrevAntenna()
{
	Antenna antenna = GetNextAntenna(GetCurrentAntenna());
	SwitchAntenna(antenna);
}

void AntennaSwitch::CloseDevice()
{
	if (ftdiDevice != NULL)
		FT_Close(ftdiDevice);
}

void AntennaSwitch::InitFTDIDevice()
{
	OpenDevice();
	SwitchAntenna(initialAntenna);
}

AntennaSwitch::Antenna AntennaSwitch::NumToAntenna(int n)
{
	Antenna antenna;
	switch (n)
	{
		case 0:
			antenna = ONE;
			break;
		case 1:
			antenna = TWO;
			break;
		case 2:
			antenna = THREE;
			break;
		case 3:
			antenna = FOUR;
			break;
		case 4:
			antenna = FIVE;
			break;
		case 5:
			antenna = SIX;
			break;
		case 6:
			antenna = SEVEN;
			break;
		case 7:
			antenna = EIGHT;
			break;
		default:
			break;
	}
	return antenna;

}
