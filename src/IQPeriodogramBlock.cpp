#include "IQPeriodogramBlock.hpp"

using namespace std;
using namespace arma;

IQPeriodogramBlock::IQPeriodogramBlock()
{

}

IQPeriodogramBlock::IQPeriodogramBlock(const IQPeriodogramBlock& prototype)
{
	spectrumBlock = prototype.spectrumBlock;
	iqtrace = prototype.iqtrace;
}

void IQPeriodogramBlock::IQToSpectrum()
{
	DetectionModule::FillSpectrumFromIQ(this->iqtrace, this->spectrumBlock);
}

vector<float> IQPeriodogramBlock::FreqProjection()
{
	return spectrumBlock.FreqProjection();
}

vector<float> IQPeriodogramBlock::TimeProjection()
{
	return spectrumBlock.TimeProjection();
}

void IQPeriodogramBlock::ToOutput()
{
	this->spectrumBlock.ToOutput();
}

int IQPeriodogramBlock::GetTimeSize()
{
	return this->spectrumBlock.GetTimeSize();
}

int IQPeriodogramBlock::GetFreqSize()
{
	return this->spectrumBlock.GetFreqSize();
}

vector<SpectrumDataBlock> IQPeriodogramBlock::GetThresholdSlicedDatablocks(float threshold)
{
	return this->spectrumBlock.GetThresholdSlicedDatablocks(threshold);
}

