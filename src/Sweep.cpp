#include "Sweep.hpp"

using namespace std;
using namespace arma;

Sweep::Sweep()
{
}

Sweep::Sweep(unsigned int traceLen, double binSize, double startFreq)
{
    this->traceLen = traceLen;
    this->binSize = binSize;
    this->startFreq = startFreq;
    sweepMax = make_shared<vector<float>>(traceLen);
    sweepMin = make_shared<vector<float>>(traceLen);

    linearized = false;
}

float Sweep::GetFreq(int i)
{
    if (i >= 0 && i < (int) sweepMax->size())
        return (startFreq + i * binSize);
    else
        return -1;
}

void Sweep::Smoothen()
{
    vector<float>& datavec = (*sweepMax);
    int windowWidth = round(datavec.size() * 0.01); // ширина окна 1% от длины наблюдения в бинах - это вполне ок)

    MathModule::MovingMax(datavec, windowWidth);
    MathModule::MovingMean(datavec, round(windowWidth/2)); // если сделать MovingMean 2 раза ,то огибающая получится более гладкая
    MathModule::MovingMean(datavec, round(windowWidth/2));
}

void Sweep::Merge(Sweep& sweep)
{
    if (sweep.getTraceLen() != this->traceLen ||
        sweep.getBinSize() != this->binSize ||
        sweep.getStartFreq() != this->startFreq)
        throw runtime_error("Cannot merge sweeps with different initial parameters");

    for (int i = 0; i < (int)traceLen; i++)
    {
        if ((*sweep.sweepMax)[i] > (*sweepMax)[i])
            (*sweepMax)[i] = (*sweep.sweepMax)[i];

        if ((*sweep.sweepMin)[i] > (*sweepMin)[i])
            (*sweepMin)[i] = (*sweep.sweepMin)[i];
    }
}

string Sweep::ToString()
{
    ostringstream stringStream;
    stringStream << traceLen << ";" << binSize << ";" << startFreq << ";";

    for(vector<float>::iterator it = sweepMax->begin(); it != sweepMax->end(); ++it)
        stringStream << *it << ";";

    return stringStream.str();
}

// dBm -> mWatt
void Sweep::Linearize()
{
    if (!linearized)
    {
        for (int i = 0; i < (int) (sweepMax->size()); i++)
            (*sweepMax)[i] = MathModule::Linearize((*sweepMax)[i]);

        linearized = true;
    }
}

// mWatt -> dBm
void Sweep::Logarithmize()
{
    if (linearized)
    {
        for (int i = 0; i < (int) (sweepMax->size()); i++)
            (*sweepMax)[i] = MathModule::Logarithmize((*sweepMax)[i]);

        linearized = false;
    }
}

Sweep Sweep::SliceByIndexes(int startInd, int endInd)
{
    Sweep slicedSweep(endInd - startInd, binSize, startFreq);
    slicedSweep.linearized = linearized;

    copy(sweepMax->begin() + startInd, sweepMax->begin() + endInd, slicedSweep.sweepMax->begin());
    copy(sweepMin->begin() + startInd, sweepMin->begin() + endInd, slicedSweep.sweepMax->begin());

    return slicedSweep;
}

void Sweep::MoveUpDown(float dBm)
{
    if (linearized == true)
        dBm = MathModule::Linearize(dBm);

    for (int i = 0; i < (int) (sweepMax->size()); i++)
    {
        (*sweepMin)[i] += dBm;
        (*sweepMin)[i] += dBm;
    }
}


float Sweep::Integrate()
{
    return Integrate(0, sweepMax->size());
}

float Sweep::Integrate(int start, int end)
{
    if (start < 0 || start >= (int) (sweepMax->size()) ||
        end < 0 || end > (int)  (sweepMax->size()) ||
        start >= end)
        throw runtime_error("Incorrect range for integration!");

    vector<float>::const_iterator first = sweepMax->begin() + start;
    vector<float>::const_iterator last = sweepMax->begin() + end;
    vector<float> Y(first, last);

    vector<float> X;
    X.reserve(end - start);
    for (int i = start; i < end ; i++)
    {
        X.push_back(GetFreq(i));
    }

    fvec armaX(X);
    fvec armaY(Y);

    return accu(trapz(armaX, armaY));
}

float Sweep::GetMax()
{
    return *max_element((*sweepMax).begin(), (*sweepMax).end());
}

float Sweep::GetMin()
{
    return *min_element((*sweepMax).begin(), (*sweepMax).end());
}

int Sweep::GetMaxIndex()
{
    return (int)(max_element((*sweepMax).begin(), (*sweepMax).end()) - (*sweepMax).begin());
}

float Sweep::GetMaxFreq()
{
    return Sweep::GetFreq(Sweep::GetMaxIndex());
}


// getters and setters
unsigned int Sweep::getTraceLen()
{
    return traceLen;
}

double Sweep::getBinSize()
{
    return binSize;
}

double Sweep::getStartFreq()
{
    return startFreq;
}
