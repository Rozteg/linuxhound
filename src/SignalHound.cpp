#include "SignalHound.hpp"

using namespace std;

int SignalHound::id = -1;
// Sweep settings
unsigned int SignalHound::traceLen = 0;
double SignalHound::binSize = 0;
double SignalHound::startFreq = 0;
// IQ trace settings
double SignalHound::bandwidth = 0;
int SignalHound::sampleRate = 0;
int SignalHound::blockLength = 0;

bbStatus SignalHound::status = bbNoError;
Sweep SignalHound::lastSweep = Sweep();
DeviceSettings SignalHound::deviceSettings = DeviceSettings();

bbStatus SignalHound::OpenDevice()
{
    status = bbOpenDevice(&id);
    if (id < 0)
	    throw runtime_error("Cannot open spectrum analyzer! " + to_string(static_cast<int>(status))); // some exception
    return status;
}

bbStatus SignalHound::ConfigureDevice(DeviceSettings& settings)
{
    SignalHound::deviceSettings = settings;

    status = bbConfigureAcquisition(id, settings.getDetector(), settings.getScale());
    status = bbConfigureCenterSpan(id, settings.getCenter(), settings.getSpan());
    status = bbConfigureLevel(id, settings.getRef(), settings.getAtten());
    cout << "Atten " << settings.getAtten() << " Status " << status << endl;
    status = bbConfigureGain(id, settings.getGain());
    cout << "Gain " << settings.getGain() << " Status " << status << endl;
    status = bbConfigureSweepCoupling(id, settings.getRBW(), settings.getVBW(),
         settings.getSweepTime(), settings.getRBWShape(), settings.getRejection());
    status = bbConfigureProcUnits(id, settings.getProcUnits());
    if (status < 0)
      throw runtime_error("Cannot configure spectrum analyzer! " + to_string(static_cast<int>(status)));
    return status;
}

bbStatus SignalHound::ConfigureDeviceIQ(DeviceSettings& settings)
{
  SignalHound::deviceSettings = settings;

  // span width is ignored here
  status = bbConfigureCenterSpan(id, settings.getCenter(), settings.getSpan());

  // Set reference level and auto gain/atten
  status = bbConfigureLevel(id, settings.getRef(), settings.getAtten());
  status = bbConfigureGain(id, settings.getGain());

  // Set a sample rate of 40.0e6 / 1 = 40.0e6 MS/s (Megasamples per second) and bandwidth of 27 MHz
  status = bbConfigureIQ(id, 1, settings.getSpan()); // TODO sample rate settings // use span as a bandwidth

  // Initialize the device for IQ streaming
  status = bbInitiate(id, BB_STREAMING, BB_STREAM_IQ);
  if(status != bbNoError) 
  {
    cout << "failed to init IQ mode" << endl;
    cout << "Status: " << status << endl;
    return status;
  }

  // Verify bandwidth and sample rate
  bbQueryStreamInfo(id, &blockLength, &bandwidth, &sampleRate);
  if (status < 0)
    throw runtime_error("Cannot Init IQ mode! " + to_string(static_cast<int>(status)));

  return status;
}

bbStatus SignalHound::ConfigureDevice()
{
    return ConfigureDevice(deviceSettings);
}

bbStatus SignalHound::InitDeviceSweep()
{
    status = bbInitiate(id, BB_SWEEPING, 0);
    status = bbQueryTraceInfo(id, &traceLen, &binSize, &startFreq);
    if (status < 0)
      throw runtime_error("Cannot init spectrum analyzer! " + to_string(static_cast<int>(status)));
    return status;
}

bbStatus SignalHound::TakeSweep()
{
    lastSweep = Sweep(traceLen, binSize, startFreq);
    status = bbFetchTrace_32f(id, traceLen, &((*lastSweep.sweepMin)[0]), &((*lastSweep.sweepMax)[0]));
    //cout << "Status: " << status << endl;
    if (status < 0)
      throw runtime_error("Cannot take Sweep! " + to_string(static_cast<int>(status)));
    return status;
}

bbStatus SignalHound::FetchRaw(float* buffer)
{
    status = bbFetchRaw(id, buffer, 0); // triggers = 0
    if (status < 0)
      throw runtime_error("Cannot take IQ Trace! " + to_string(static_cast<int>(status)));
    return status;
}

bbStatus SignalHound::CloseDevice()
{
    if (id >= 0)
        status = bbCloseDevice(id);
    return status;
}
