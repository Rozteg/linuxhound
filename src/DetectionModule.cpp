#include "DetectionModuleFilter.hpp"
#include "DetectionModuleAdiag.hpp"
#include "DetectionModule.hpp"

using namespace std;
using namespace arma;

sp::IIR_filt<float, float, float> DetectionModule::lowPassFilter1;
sp::IIR_filt<float, float, float> DetectionModule::lowPassFilter2;
AntennaDiagram DetectionModule::ADiag;

void DetectionModule::CreateFilters()
{
    fvec coefb1 = {1, -1.9625, 1};
    fvec coefa1 = {-1.9782, 0.9765};
    fvec coefb2 = {1, -1.9931, 1};
    fvec coefa2 = {-1.9910, 0.9920};
    DetectionModule::lowPassFilter1.set_coeffs(coefb1, coefa1);
    DetectionModule::lowPassFilter2.set_coeffs(coefb2, coefa2);
    cout << "low pass filter designed" << endl;
}

void DetectionModule::CreateAdiagram(double step, double sigma, double offset)
{
    AntennaDiagram aDiag(sigma, step, offset); 
    DetectionModule::ADiag = aDiag;
}

void DetectionModule::Filter(vector<float>& signal)
{
    fvec in1(signal);
    fvec out1;
    fvec out2;

    out1 = DetectionModule::lowPassFilter1.filter(in1);
    out2 = DetectionModule::lowPassFilter2.filter(out1);
    signal = conv_to<vector<float>>::from(out2);
}

void DetectionModule::InitDetector()
{
    DetectionModule::CreateFilters();
    //DetectionModule::CreateAdiagram(2.5, 15, 0); // sigma = 25, step = 5
    DetectionModule::CreateAdiagram(5, 26, 0); //(шаг дискретизации (в градусах), сигма(ширина кривой гаусса), смещение влево-вправо в градусах)
}

/* попытка упростить/ускорить определение камеры, здесь нет смещения сигнала на центральную частоту и нет фильтрации, работает */
int DetectionModule::SimpleDetectCamera(IQTrace& iqtrace)
{
    // IQ vector is "buffer", don't forget it
    vector<float> bigI((iqtrace.buffer->size() / 2));
    vector<float> bigQ((iqtrace.buffer->size() / 2));
    for (long unsigned int i = 0; i < iqtrace.buffer->size(); i+=2)
    {
        bigI[i/2] = (*iqtrace.buffer)[i];
        bigQ[i/2] = (*iqtrace.buffer)[i+1];
    }

    // spectrogam
    vector<vector<float>> spectrogram;
    long unsigned int halfInterval = FFT_WINDOW_WIDTH_HALF_INTERVAL;
    long unsigned int fullInterval = halfInterval * 2;

    // length of the interval is 2048 and we take them with 50% overlapping
    long unsigned int intervalsQuantity = ((iqtrace.buffer->size() / 2) / fullInterval) * 2 - 1; // throw everything that doesn't fit

    if (((iqtrace.buffer->size() / 2) / fullInterval) == 0)
        throw runtime_error("IQ block of incorrect length is given!");

    // iterators for large IQ block
    vector<float>::iterator startIterI = bigI.begin();
    vector<float>::iterator endIterI = startIterI + fullInterval;

    vector<float>::iterator startIterQ = bigQ.begin();
    vector<float>::iterator endIterQ = startIterQ + fullInterval;

    vector<float> hammingCoefs(fullInterval);
    MathModule::HammingCoefs(hammingCoefs);


    vector<float> I;
    vector<float> Q;
    vector<float> fftRe;
    vector<float> fftIm;
    vector<float> fftAbs(fullInterval);

    fftwf_complex* in = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*fullInterval);
    fftwf_complex* out = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*fullInterval);
    fftwf_plan my_plan = fftwf_plan_dft_1d(fullInterval, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    for (long unsigned int i = 0 ; i < intervalsQuantity ; i++)
    {
        I = vector<float>(startIterI, endIterI);
        Q = vector<float>(startIterQ, endIterQ);


        for (long unsigned int k = 0 ; k < hammingCoefs.size() ; k++)
        {
            I[k] *= hammingCoefs[k];
            Q[k] *= hammingCoefs[k];

            in[k][0] = I[k];
            in[k][1] = Q[k];
        }

        fftwf_execute(my_plan);
        //MathModule::FFT(I, Q, fftRe, fftIm);

        fftRe.resize(I.size());
        fftIm.resize(Q.size());

        for (long unsigned int i = 0; i < I.size() ; i++)
        {
            fftRe[i] = out[i][0];
            fftIm[i] = out[i][1];
        }

        // calculating absolute value and centering the domain
        // www.fftw.org/faq/section3
        // question 3.11
        for (long unsigned int n = 0 ; n < fftRe.size() ; n++)
        {
        // same result as PrepareCenteredFFTData();
        // but I prefer this way
            long unsigned int index;
            if (n >= fftRe.size()/2)
                index = n - fftRe.size()/2;
            else
                index = n + fftRe.size()/2;
    
            complex<float> num(fftRe[n], fftIm[n]);
            fftAbs[index] = abs(num);
        }

        spectrogram.push_back(fftAbs);

        startIterI += halfInterval;
        endIterI += halfInterval;
        startIterQ += halfInterval;
        endIterQ += halfInterval;
    }

    fftwf_destroy_plan(my_plan);
    fftwf_free(in);
    fftwf_free(out);

    // debug // works
    //for (long unsigned int i = 0 ; i < spectrogram.size() ; i++)
    //  MathModule::VecToOutput(spectrogram[i]);

    // Next - max hold 750 microseconds
    // 0.025 usec
    float binTime = 0.000000025; // usec, see Signal Hound documentation (bbFetchRaw, bbGetIQ)
    float maxHoldTime = 0.00075;
    long unsigned int frameLength = static_cast<long unsigned int>(maxHoldTime / (binTime * fullInterval)); // 750 / 1 spectrogram segment
    long unsigned int frames = spectrogram.size() / frameLength;

    //cout << "Frame length: " << frameLength << endl;
    //cout << "Frames: " << frames << endl;

    vector<float> frame(fullInterval);
    vector<vector<float>> periodogram;

    for (long unsigned int i = 0; i < frames; i++)
    {
        frame = spectrogram[i * frameLength];
        for (long unsigned int n = (i * frameLength + 1) ; n < (i * frameLength + frameLength) ; n++)
        {
            for (long unsigned int k = 0; k < spectrogram[n].size(); k++)
            {
                if (frame[k] < spectrogram[n][k])
                    frame[k] = spectrogram[n][k];
            }
        }

        periodogram.push_back(frame);
    }

    // debug //works
    //for (long unsigned int i = 0 ; i < periodogram.size() ; i++)
    //  MathModule::VecToOutput(periodogram[i]);

    // periodogram [timeIndex][FreqIndex]
    //looking for time and freq with max power
    vector<float> timeMaxHold(periodogram.size());

    // preparing times MaxHold
    for (long unsigned int i = 0; i < periodogram.size(); i++)
        timeMaxHold[i] = periodogram[i][0];

    for (long unsigned int n = 0; n < periodogram[0].size(); n++) // full interval
    {
        for (long unsigned int i = 0; i < periodogram.size(); i++)
        {
            if (timeMaxHold[i] < periodogram[i][n])
                timeMaxHold[i] = periodogram[i][n];
        }
    }


    //MathModule::VecToOutput(timeMaxHold);

    vector<float> filteredSignalEnergy(timeMaxHold);
    //autocorrelation
    filteredSignalEnergy = MathModule::Autocorrelation(timeMaxHold);

    // autocorrelation debug
    //MathModule::VecToOutput(filteredSignalEnergy);

    // search peak distance
    long unsigned int binsPerFrame = static_cast<long unsigned int>(maxHoldTime / binTime / 2);
    float stepTime =  binsPerFrame * binTime;
    float overallTime = filteredSignalEnergy.size() * stepTime;
    float peakPeriod = 0.021; // sync-pulse period is about 21 millisecond
    vector<int> indices;

    Util::findPeaks(filteredSignalEnergy, indices);

    for (long unsigned int i = 0; i < indices.size(); i++)
        cout << indices[i] << endl;

    int intervals = 0;
    sort(indices.begin(), indices.end());
    for (long unsigned i = 0; i < indices.size() - 1; i++)
    {
        float distance;
        distance = (indices[i+1] - indices[i]) * stepTime;
        //cout << "Distance: " << distance << endl;
        if ((distance < 0.022) && (distance > 0.019))
            intervals++;
    }

    //cout << "Intervals Detected: " << intervals << endl;
    if (intervals > 0)
        cout << "Camera!" << endl;
    else
        cout << "NoCam" << endl;

    return intervals;
}

/* Функция определяет, является ли сигнал камерой */
int DetectionModule::DetectCamera(IQTrace& iqtrace)
{
    // IQ vector is "buffer", don't forget it
    vector<float> bigI((iqtrace.buffer->size() / 2));
    vector<float> bigQ((iqtrace.buffer->size() / 2));
    for (long unsigned int i = 0; i < iqtrace.buffer->size(); i+=2)
    {
        bigI[i/2] = (*iqtrace.buffer)[i];
        bigQ[i/2] = (*iqtrace.buffer)[i+1];
    }

    // spectrogam
    vector<vector<float>> spectrogram;
    long unsigned int halfInterval = 1024;
    long unsigned int fullInterval = halfInterval * 2;

    // length of the interval is 2048 and we take them with 50% overlapping
    long unsigned int intervalsQuantity = ((iqtrace.buffer->size() / 2) / fullInterval) * 2 - 1; // throw everything that doesn't fit

    if (((iqtrace.buffer->size() / 2) / fullInterval) == 0)
        throw runtime_error("IQ block of incorrect length is given!");

    // iterators for large IQ block
    vector<float>::iterator startIterI = bigI.begin();
    vector<float>::iterator endIterI = startIterI + fullInterval;

    vector<float>::iterator startIterQ = bigQ.begin();
    vector<float>::iterator endIterQ = startIterQ + fullInterval;

    vector<float> hammingCoefs(fullInterval);
    MathModule::HammingCoefs(hammingCoefs);

    vector<float> I;
    vector<float> Q;
    vector<float> fftRe;
    vector<float> fftIm;
    vector<float> fftAbs(fullInterval);

    fftwf_complex* in = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*fullInterval);
    fftwf_complex* out = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*fullInterval);
    fftwf_plan my_plan = fftwf_plan_dft_1d(fullInterval, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    for (long unsigned int i = 0 ; i < intervalsQuantity ; i++)
    {
        I = vector<float>(startIterI, endIterI);
        Q = vector<float>(startIterQ, endIterQ);


        for (long unsigned int k = 0 ; k < hammingCoefs.size() ; k++)
        {
            I[k] *= hammingCoefs[k];
            Q[k] *= hammingCoefs[k];

            in[k][0] = I[k];
            in[k][1] = Q[k];
        }

        fftwf_execute(my_plan);
        //MathModule::FFT(I, Q, fftRe, fftIm);

        fftRe.resize(I.size());
        fftIm.resize(Q.size());

        for (long unsigned int i = 0; i < I.size() ; i++)
        {
            fftRe[i] = out[i][0];
            fftIm[i] = out[i][1];
        }

        // calculating absolute value and centering the domain
        // www.fftw.org/faq/section3
        // question 3.11
        for (long unsigned int n = 0 ; n < fftRe.size() ; n++)
        {
        // same result as PrepareCenteredFFTData();
        // but I prefer this way
            long unsigned int index;
            if (n >= fftRe.size()/2)
                index = n - fftRe.size()/2;
            else
                index = n + fftRe.size()/2;
    
            complex<float> num(fftRe[n], fftIm[n]);
            fftAbs[index] = abs(num);
        }

        spectrogram.push_back(fftAbs);

        startIterI += halfInterval;
        endIterI += halfInterval;
        startIterQ += halfInterval;
        endIterQ += halfInterval;
    }

    fftwf_destroy_plan(my_plan);
    fftwf_free(in);
    fftwf_free(out);

    // debug // works
    //for (long unsigned int i = 0 ; i < spectrogram.size() ; i++)
    //  MathModule::VecToOutput(spectrogram[i]);

    // Next - max hold 750 microseconds
    // 0.025 usec
    float binTime = BB_IQ_TIME_DELTA; // 0.000000025 sec, see Signal Hound documentation (bbFetchRaw, bbGetIQ)
    float maxHoldTime = 0.00075;
    long unsigned int frameLength = static_cast<long unsigned int>(maxHoldTime / (binTime * fullInterval)); // 750 / 1 spectrogram segment
    long unsigned int frames = spectrogram.size() / frameLength;

    //cout << "Frame length: " << frameLength << endl;
    //cout << "Frames: " << frames << endl;

    vector<float> frame(fullInterval);
    vector<vector<float>> periodogram;

    for (long unsigned int i = 0; i < frames; i++)
    {
        frame = spectrogram[i * frameLength];
        for (long unsigned int n = (i * frameLength + 1) ; n < (i * frameLength + frameLength) ; n++)
        {
            for (long unsigned int k = 0; k < spectrogram[n].size(); k++)
            {
                if (frame[k] < spectrogram[n][k])
                    frame[k] = spectrogram[n][k];
            }
        }

        periodogram.push_back(frame);
    }

    // debug //works
    //for (long unsigned int i = 0 ; i < periodogram.size() ; i++)
    //  MathModule::VecToOutput(periodogram[i]);

    // periodogram [timeIndex][FreqIndex]
    //looking for time and freq with max power
    vector<float> timeMaxHold(periodogram.size());

    // preparing times MaxHold
    for (long unsigned int i = 0; i < periodogram.size(); i++)
        timeMaxHold[i] = periodogram[i][0];

    for (long unsigned int n = 0; n < periodogram[0].size(); n++) // full interval
    {
        for (long unsigned int i = 0; i < periodogram.size(); i++)
        {
            if (timeMaxHold[i] < periodogram[i][n])
                timeMaxHold[i] = periodogram[i][n];
        }
    }


    // timeMaxHold is ready. Now looking for lower spectrum half
    vector<float> powerMaxHold(periodogram[0].size());

    // preparing powers MaxHold
    for (long unsigned int i = 0; i < periodogram[0].size(); i++)
        powerMaxHold[i] = periodogram[0][i];

    for (long unsigned int i = 0; i < periodogram.size(); i++) // full interval
    {
        for (long unsigned int n = 0; n < periodogram[i].size(); n++)
        {
            if (powerMaxHold[n] < periodogram[i][n])
                powerMaxHold[n] = periodogram[i][n];
        }
    }

    //

    // debug // works
    //MathModule::VecToOutput(powerMaxHold);

    // cutting off zeros
    float freqBinSize = iqtrace.getSampleRate() / fullInterval;
    long unsigned int distanceToCut = static_cast<long unsigned int>((float(iqtrace.getSampleRate() - iqtrace.getBandwidth())  / 2) / freqBinSize);
    // cutting off zeros and calculating mean
    float MeanMaxHold = accumulate(powerMaxHold.begin() + distanceToCut, powerMaxHold.end() - distanceToCut, 0.0) / (powerMaxHold.size() - (distanceToCut * 2));
    long unsigned int passIndexForward = 0;
    long unsigned int passIndexBackward = 0;

    for(long unsigned int i = 0; MeanMaxHold > powerMaxHold[i]; i++)
        passIndexForward = i;

    for(long unsigned int i = (powerMaxHold.size() - 1); MeanMaxHold > powerMaxHold[i]; i--)
        passIndexBackward = i;

    long unsigned int middleIndex = passIndexForward + (passIndexBackward - passIndexForward) / 2;
    //float firstFreq = (passIndexForward - timeMaxHold.size()/2) * freqBinSize;
    //float secondFreq = (passIndexBackward - timeMaxHold.size()/2) * freqBinSize;
    float band = (passIndexBackward - passIndexForward) * freqBinSize;

    cout << "bandpass " << band << endl;

    if (band < 6.0e6 && band > 5.0e6)
    cout << "probably, camera!" << endl;

    long unsigned int timeIndex = distance(timeMaxHold.begin(), max_element(timeMaxHold.begin(), timeMaxHold.end())); // may be bottleneck if container is not vector
    // looking for max freq
    //float freqBinSize = sampleRate / fullInterval;
    long unsigned int freqIndex = distance(periodogram[timeIndex].begin(), max_element(periodogram[timeIndex].begin(), periodogram[timeIndex].begin() + middleIndex));
    //long unsigned int freqIndex = distance(periodogram[timeIndex].begin(), max_element(periodogram[timeIndex].begin(), periodogram[timeIndex].end()));
    float maxPowerFreq = (long signed int)(freqIndex - periodogram[timeIndex].size()/2) * freqBinSize; // relative freq

    cout << maxPowerFreq << endl;
    //debug // works
    //MathModule::VecToOutput(timeMaxHold);

    // shift signal freq to max power freq
    MathModule::ShiftFreq(bigI, bigQ, maxPowerFreq, binTime);
    cout << "shifted" << endl;
    // debug //works
    //MathModule::VecToOutput(bigI);
    //MathModule::VecToOutput(bigQ);

    //design filter // went to Math module, call in controller
    //filter
    //clock_t begin_f = clock();
    DetectionModule::Filter(bigI);
    DetectionModule::Filter(bigQ);

    //clock_t end_f = clock();
    //cout << "Filtering time: " << float(end_f - begin_f) / CLOCKS_PER_SEC << endl;

    // power
    long unsigned int binsPerFrame = static_cast<long unsigned int>(maxHoldTime / binTime);
    vector<float> filteredSignalEnergy;
    float powerSum = 0;
    for (long unsigned i = 0; i < bigI.size(); i++)
    {
        powerSum += pow(bigI[i], 2) + pow(bigQ[i], 2);
        if(((i + 1) % binsPerFrame) == 0)
        {
            filteredSignalEnergy.push_back(powerSum);
            powerSum = 0;
        }
    }
    filteredSignalEnergy.push_back(powerSum);

    // filteredSignal output
    //MathModule::VecToOutput(filteredSignalEnergy);

    //autocorrelation
    filteredSignalEnergy = MathModule::Autocorrelation(filteredSignalEnergy);

    // autocorrelation debug
    //MathModule::VecToOutput(filteredSignalEnergy);

    // search peak distance
    float stepTime =  binsPerFrame * binTime;
    float overallTime = filteredSignalEnergy.size() * stepTime;
    float peakPeriod = 0.021; // sync-pulse period is about 21 millisecond
    long unsigned int periodsQuantity = static_cast<long unsigned int>(overallTime / peakPeriod);
    vector<int> indices;

    Util::findPeaks(filteredSignalEnergy, indices);

    for (long unsigned int i = 0; i < indices.size(); i++)
        cout << indices[i] << endl;

    int intervals = 0;
    sort(indices.begin(), indices.end());
    for (long unsigned i = 0; i < indices.size() - 1; i++)
    {
        float distance;
        distance = (indices[i+1] - indices[i]) * stepTime;
        //cout << "Distance: " << distance << endl;
        if ((distance < 0.022) && (distance > 0.019))
            intervals++;
    }

    //cout << "Intervals Detected: " << intervals << endl;
    if (intervals > 0)
        cout << "Camera!" << endl;
    else
        cout << "NoCam" << endl;

    return intervals;
}


float DetectionModule::CalculateDirection(SweepCircle& sweepCircle)
{
    vector<Sweep>& sweeps = sweepCircle.sweeps;

    for (int i = 0; i < (int)sweeps.size(); i++)
        sweeps[i].Linearize();

    //float threshold = pow(10, (-65.0/10)); // -75 dbm
    //bool isSignal = false;

    int l = 0;
    int maxAntenna = 0;
    float power = sweeps[l].GetMax();
    for (l = 0; l < (int)sweeps.size(); l++)
    {
        float p = sweeps[l].GetMax();
        if (p > power)
        {
            power = p;
            maxAntenna = l;
        }

        //if (p >= threshold)
        //    isSignal = true;
    }

    // no signal
    //if (!isSignal)
    //    return -1;

    int prevAntenna = maxAntenna - 1 < 0 ? AntennaSwitch::ANTENNAS_QUANTITY - 1 : maxAntenna - 1;
    int nextAntenna = maxAntenna + 1 >= AntennaSwitch::ANTENNAS_QUANTITY ? 0 : maxAntenna + 1;
    int secondMaxAntenna;

    int direction = 0;
    if (sweeps[nextAntenna].GetMax() > sweeps[prevAntenna].GetMax())
    {
        secondMaxAntenna = nextAntenna;
        direction = 1;
    }
    else
    {
        secondMaxAntenna = prevAntenna;
        direction = -1;
    }

    float A = sweeps[maxAntenna].GetMax();
    float B = sweeps[secondMaxAntenna].GetMax();


    float angle = maxAntenna * 45.0 + direction * DetectionModule::ADiag.GetDirection(A, B) + DetectionModule::ADiag.getOffset();

    if (angle < 0)
        angle += 360.0;

    if (angle > 360)
        angle -= 360.0;

    cout << "Max antenna: " << maxAntenna << endl;
    cout << "Max antenna angle: " << maxAntenna * 45 << endl;
    cout << "Direction: " << direction << endl;
    cout << "Max amp: " << 10 * log10(A) << " dBm" << endl;
    cout << "Angle: " << angle << endl;
    cout << "Angle offset: " << DetectionModule::ADiag.getOffset() << "\n----" << endl;

    return angle;
}

void DetectionModule::MultipleDetect(SweepCircle& scannedCircle, SweepCircle& initialBackgroundCircle, vector<SweepCircle>& slicedCircles)
{
    for (int i = 0; i < scannedCircle.sweeps.size(); i++)
    {
        vector<float>& scannedVector = *(scannedCircle.sweeps[i].sweepMax);
        vector<float>& backgroundVector = *(initialBackgroundCircle.sweeps[i].sweepMax);
        int startInd = 0;
        int endInd = -1;
        auto prevVal = scannedVector[0];
        for (int n = 1; n < scannedVector.size(); n++)
        {
            if (scannedVector[n] > backgroundVector[n] && scannedVector[n-1] < backgroundVector[n-1])
                startInd = n - 1;

            if ((scannedVector[n] < backgroundVector[n] && scannedVector[n-1] > backgroundVector[n-1]))
                endInd = n;
            

            if (endInd != -1)
            {
                // костыль // если 3 или меньше бинов - это не сигнал, а помеха, её не рассматриваем
                if ((endInd - startInd) > 3)
                {
                    cout << "MultipleDetect1 " << startInd << " " << endInd << endl;
                    slicedCircles.push_back(scannedCircle.SliceByIndexes(startInd, endInd));
                }

                endInd = -1;
                startInd = 0;
            }
            prevVal = scannedVector[n];
        }
        if (startInd != 0)
            slicedCircles.push_back(scannedCircle.SliceByIndexes(startInd, scannedVector.size() - 1));
    }
    /* после канители выше мы получили множество кусочков сигнала, которые предположительно являютсяразными сигналами */
    /* чтобы было совсем красиво, можно сравнить эти кусочки сигнала по частотам и амплитудам, чтобы убрать посторяющиеся сигналы с различных антенн */
    /* но это уже совсем другая история */
}



/* Витин скрипт, переведённый в лоб, сам до конца не понял */
float DetectionModule::CalculateDirectionStatistically(SweepCircle& sweepCircle)
{
    vector<Sweep>& sweeps = sweepCircle.sweeps;
    bool signalExistence = true;
    fmat sigMat;

    for (long unsigned int j = 0; j < sweeps.size(); j++)
    {
        vector<float>& sweepVec = *sweeps[j].sweepMax;

        MathModule::WaveletDenoise(sweepVec); //  s = wden(d(k, 5:(4+elements-1)),'sqtwolog','h','mln',5,'sym8');

        //MathModule::VecToOutput(sweepVec);

        //s11 = diff(smooth(s, 0.1, 'lowess'));
        //s22 = diff(smooth(s, 0.1, 'lowess'),2); % diff in arma

        fvec s11(MathModule::SmoothVector(sweepVec, 5));
        fvec s22(MathModule::SmoothVector(sweepVec, 5));
        s11 = diff(s11);
        s22 = diff(s22, 2);

        int smax = s11.index_max();
        int smin = s11.index_min();
        int scenter = (smax+smin)/2;

        s22 = s22.subvec(0, scenter);
        
        smax = s22.index_max(); 
        int width = smax;
        // w1 = exp(-abs((1:numel(s))-scenter)/1000);

        vector<float> w1(sweepVec.size());
        for (int i = 0; i < w1.size(); i++)
            w1[i] = std::exp( float((-1) * abs(i+1 - scenter)) / 1000);

        float w1min = *min_element(w1.begin(), w1.end());
        for (int i = 0; i < w1.size(); i++)
            w1[i] -= w1min;

        float sweepMin = *min_element(sweepVec.begin(), sweepVec.end());
        vector<float> y(sweepVec.size());
        for (int i = 0; i < y.size(); i++)
            y[i] = (sweepVec[i] - sweepMin) * w1[i];

        fvec armaY(y);
        fvec armaZeros = zeros<fvec>(y.size());
        cx_fvec armaCompY(armaY, armaZeros);
        armaY = real(fft(ifft(armaCompY),1000));

        /*
        if(mean(y)<=std(y) || mean(y)<1) % mean in arma
            signalExistence=false;
            disp([num2str(j) ' ' num2str(k) ' no signal']);
        end
        s2=[s2 y'];
        */

        if(mean(armaY) <= stddev(armaY) || mean(armaY) < 1)
            signalExistence=false;
        
        sigMat.insert_cols(j, armaY);
    }

    //cout << sigMat << endl;
    //cout << sigMat.size() << endl;

    //if (signalExistence)
    //{
        cout << "Signal" << endl;
        fmat covm = cov(sigMat.t());

        fmat U;
        fvec ev;
        fmat V;

        svd(U,ev,V,covm);

        int targetscount = 0;
        fvec evProp= 100*ev / accu(ev);

        for (int i = 0; i < evProp.size(); i++)
        {
            if (evProp[i] > 10)
                targetscount += 1;
        }

        cout << "targets: " << targetscount << endl;

        for (int t = 0; t < targetscount; t++)
        {
            fvec ev1 = zeros<fvec>(ev.size());
            ev1[t] = ev[t];
            fmat covm_t = U * diagmat(ev1) * V.t();

            fvec w = zeros<fvec>(sweeps.size());
            fvec sigsum = zeros<fvec>(sweeps.size());
            int ksum = 0;

            for (int k = 0; k < sweeps.size(); k++)
            {
                fvec y = covm_t.row(k).t();
                sigsum[k] = std::exp(accu(y-min(y))/100);
                ksum += sigsum[k];
            }

            w = sigsum / ksum;
            for (int n = 0; n < w.size(); n++)
            {
                if (w[n]>mean(w))
                    w[n] *= w[n];
            }
            w = w / accu(w);

            for (int n = 1; n <= sweeps.size(); n++)
            {
                w[n-1] *= n;
            }

            cout << w << endl;

            double an = accu(w);
            int bearing = round((an-floor(an))*45+(floor(an))*45);
            cout << "bearing " << bearing << endl;
        }
    //}

    return 0;
}


void DetectionModule::FillSpectrumFromIQ(IQTrace& iqtrace, SpectrumDataBlock& spectrumBlock)
{
    // spectrogam
    vector<vector<float>>& spectrogram = *(spectrumBlock.spectrogram);
    long unsigned int halfInterval = 1024; 
    long unsigned int fullInterval = halfInterval * 2; 

    // length of the interval is 2048 and we take them with 50% overlapping
    long unsigned int intervalsQuantity = ((iqtrace.buffer->size() / 2) / fullInterval) * 2 - 1; // throw everything that doesn't fit

    if (((iqtrace.buffer->size() / 2) / fullInterval) == 0)
        throw runtime_error("IQ block of incorrect length is given!");

    // iterator for IQ block
    vector<float>::iterator startIter = iqtrace.buffer->begin();

    vector<float> hammingCoefs(fullInterval);
    MathModule::HammingCoefs(hammingCoefs);

    vector<float> fftRe(fullInterval);
    vector<float> fftIm(fullInterval);
    vector<float> fftAbs(fullInterval);

    fftwf_complex* in = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*fullInterval);
    fftwf_complex* out = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*fullInterval);
    fftwf_plan my_plan = fftwf_plan_dft_1d(fullInterval, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    for (long unsigned int i = 0 ; i < intervalsQuantity ; i++)
    {
        for (long unsigned int k = 0 ; k < hammingCoefs.size() ; k++)
        {
            in[k][0] = (*startIter) * hammingCoefs[k];
            in[k][1] = (*(startIter + 1)) * hammingCoefs[k];

            startIter += 2; // +2 because of complex number, do not forget
        }
        startIter -= (hammingCoefs.size()/2) * 2;

        fftwf_execute(my_plan);

        for (long unsigned int m = 0; m < fullInterval ; m++)
        {
            fftRe[m] = out[m][0];
            fftIm[m] = out[m][1];
        }

        // calculating absolute value and centering the domain
        // www.fftw.org/faq/section3
        // question 3.11
        for (long unsigned int n = 0 ; n < fftRe.size() ; n++)
        {
        // same result as PrepareCenteredFFTData();
        // but I prefer this way
            long unsigned int index;
            if (n >= fftRe.size()/2)
                index = n - fftRe.size()/2;
            else
                index = n + fftRe.size()/2;
    
            complex<float> num(fftRe[n], fftIm[n]);
            fftAbs[index] = abs(num);
        }

        spectrumBlock.PushToSpectrogram(fftAbs);
        //spectrogram.push_back(fftAbs);
    }

    fftwf_destroy_plan(my_plan);
    fftwf_free(in);
    fftwf_free(out);

    // debug // works
    //for (long unsigned int i = 0 ; i < spectrogram.size() ; i++)
    //  MathModule::VecToOutput(spectrogram[i]);

    // Next - max hold 750 microseconds
    // 0.025 usec
    float binTime = BB_IQ_TIME_DELTA; //0.000000025; // usec, see Signal Hound documentation (bbFetchRaw, bbGetIQ)
    float maxHoldTime = 0.00075;
    long unsigned int frameLength = static_cast<long unsigned int>(maxHoldTime / (binTime * fullInterval)); // 750 / 1 spectrogram segment
    long unsigned int frames = spectrogram.size() / frameLength;

    //cout << "Frame length: " << frameLength << endl;
    //cout << "Frames: " << frames << endl;

    vector<float> frame(fullInterval);
    //vector<vector<float>> periodogram;

    for (long unsigned int i = 0; i < frames; i++)
    {
        frame = spectrogram[i * frameLength];
        for (long unsigned int n = (i * frameLength + 1) ; n < (i * frameLength + frameLength) ; n++)
        {
            for (long unsigned int k = 0; k < spectrogram[n].size(); k++)
            {
                if (frame[k] < spectrogram[n][k])
                    frame[k] = spectrogram[n][k];
            }
        }

        spectrumBlock.PushToPeriodogram(frame);
    }
    spectrumBlock.timeStep = maxHoldTime;
    spectrumBlock.frequencyStep = BB_IQ_SAMPLES_PER_SEC / spectrogram[0].size();
    spectrumBlock.centerFreq = iqtrace.getCenterFreq();
}
