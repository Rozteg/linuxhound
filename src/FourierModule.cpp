#include "FourierModule.hpp"

using namespace std;

long unsigned int FourierModule::sizeN = 0;
fftw_complex* FourierModule::in;
fftw_complex* FourierModule::out;
fftw_plan FourierModule::my_plan;

void FourierModule::PrepareFourier(long unsigned int sizeToPrepare)
{
    if (sizeToPrepare != FourierModule::sizeN)
    {
		FourierModule::FreeFourier();
	
		FourierModule::in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*sizeToPrepare);
		FourierModule::out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*sizeToPrepare);
		FourierModule::my_plan = fftw_plan_dft_1d(sizeToPrepare, FourierModule::in, FourierModule::out, FFTW_FORWARD, FFTW_ESTIMATE);
		FourierModule::sizeN = sizeToPrepare;
    }
}

void FourierModule::ExecuteFourier()
{
    if (FourierModule::sizeN != 0)
	fftw_execute(FourierModule::my_plan);
}

void FourierModule::FreeFourier()
{
    if (FourierModule::sizeN != 0)
    {
		fftw_destroy_plan(FourierModule::my_plan);
		fftw_free(FourierModule::in);
		fftw_free(FourierModule::out);
		FourierModule::sizeN = 0;
    }
}
