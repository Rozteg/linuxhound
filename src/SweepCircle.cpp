#include "SweepCircle.hpp"

using namespace std;

void SweepCircle::Add(Sweep sweep)
{
	sweeps.push_back(sweep);
}

void SweepCircle::Merge(SweepCircle sweepCircle)
{
	for (int i = 0; i < (int)sweeps.size(); i++)
		sweeps[i].Merge(sweepCircle.sweeps[i]);
}

void SweepCircle::Smoothen()
{
	for (int i = 0; i < (int)sweeps.size(); i++)
		sweeps[i].Smoothen();
}

void SweepCircle::MergeAverage(vector<SweepCircle>& sweepCircleVec)
{
	for (int n = 0; n < (int)sweeps.size(); n++)
	{
		for (int i = 0; i < (int)(*sweeps[0].sweepMax).size(); i++)
		{
			(*sweeps[n].sweepMax)[i] = 0;
			for (int k = 0; k < (int)sweepCircleVec.size(); k++)
				(*sweeps[n].sweepMax)[i] += (*sweepCircleVec[k].sweeps[n].sweepMax)[i];

			(*sweeps[n].sweepMax)[i] /= sweepCircleVec.size();
		}
	}
}

string SweepCircle::ToString()
{
	ostringstream stringStream;
	for (int i = 0; i < (int)sweeps.size(); i++)
		stringStream << i << ";" << sweeps[i].ToString() << "\n";

	return stringStream.str();
}

string SweepCircle::MaxesToString()
{
	ostringstream stringStream;
	for (int i = 0; i < (int)sweeps.size(); i++)
	{
		stringStream << sweeps[i].GetMax();
		if (i < ((int)sweeps.size() - 1) )
			stringStream << ';';
	}

	return stringStream.str();
}

int SweepCircle::MaxPowerDirection()
{
	int i = 0;
	int antenna = 0;
	float power = sweeps[i].GetMax();
	for (i = 0; i < (int)sweeps.size(); i++)
	{
		float p = sweeps[i].GetMax();
		if (p > power)
		{
			power = p;
			antenna = i;
		}
	}

	return antenna;
}

void SweepCircle::Normalize()
{

	for (int i = 0; i < (int)sweeps.size(); i++)
		sweeps[i].Linearize();

    vector<float> vecOfMax;
    vector<float> vecOfMin;
    vecOfMax.reserve((int)sweeps.size());
    vecOfMin.reserve((int)sweeps.size());

    for (int i = 0; i < (int)sweeps.size(); i++)
    {
		vecOfMax.push_back(sweeps[i].GetMax());
		vecOfMin.push_back(sweeps[i].GetMin());
    }

    float max = *max_element(vecOfMax.begin(), vecOfMax.end());
    float min = *min_element(vecOfMin.begin(), vecOfMin.end());

    for (int i = 0; i < (int)sweeps.size(); i++)
    {
		MathModule::NormZeroToOne((*(sweeps[i].sweepMax)), max, min);
    }
}

float SweepCircle::CalculateDirectionGaussian(AntennaDiagram& aDiag)
{
	//Normalize();
	int maxAntenna = MaxPowerDirection();

	int prevAntenna = maxAntenna - 1 < 0 ? AntennaSwitch::ANTENNAS_QUANTITY - 1 : maxAntenna - 1;
	int nextAntenna = maxAntenna + 1 >= AntennaSwitch::ANTENNAS_QUANTITY ? 0 : maxAntenna + 1;
	int secondMaxAntenna;

	int direction = 1;
	if (sweeps[nextAntenna].GetMax() > sweeps[prevAntenna].GetMax())
	{
		secondMaxAntenna = nextAntenna;
	}
	else
	{
		secondMaxAntenna = prevAntenna;
		direction = -1;
	}

	float A = sweeps[maxAntenna].GetMax();
	float B = sweeps[secondMaxAntenna].GetMax();


	float angle = maxAntenna * 45.0 + direction * aDiag.GetDirection(A, B) + aDiag.getOffset();
	

	if (angle < 0)
	    angle += 360.0;

	if (angle > 360)
	    angle -= 360.0;

	cout << "Max antenna: " << maxAntenna << endl;
	cout << "Max antenna angle: " << maxAntenna * 45 << endl;
	cout << "Direction: " << direction << endl;
	cout << "Max amp: " << A << " dBm" << endl;
	cout << "Angle: " << angle << endl;
	cout << "Angle offset: " << aDiag.getOffset() << "\n----" << endl;

	return angle;
}

float SweepCircle::CalculateDirectionGaussianAverage(AntennaDiagram& aDiag, vector<SweepCircle>& circleVector)
{
	vector<float> squares;
	
	for (int i = 0; i < (int) circleVector.size(); i++)
	{
	    circleVector[i].Normalize();
	}

	for (int i = 0; i < AntennaSwitch::ANTENNAS_QUANTITY; i++)
	{
	    float sq = 0;
	    int j = 0;
	    for (; j < (int) circleVector.size(); j++)
	    {
			sq += circleVector[j].sweeps[i].Integrate();
	    }
	    squares.push_back(sq/j);
	}

	
	int maxAntenna = distance(squares.begin(), max_element(squares.begin(), squares.end()));

	int prevAntenna = maxAntenna - 1 < 0 ? AntennaSwitch::ANTENNAS_QUANTITY - 1 : maxAntenna - 1;
	int nextAntenna = maxAntenna + 1 >= AntennaSwitch::ANTENNAS_QUANTITY ? 0 : maxAntenna + 1;
	int secondMaxAntenna;

	int direction = 1;
	if (squares[nextAntenna] > squares[prevAntenna])
	{
		secondMaxAntenna = nextAntenna;
	}
	else
	{
		secondMaxAntenna = prevAntenna;
		direction = -1;
	}

	float A = squares[maxAntenna];
	float B = squares[secondMaxAntenna];


	float angle = maxAntenna * 45.0 + direction * aDiag.GetDirection(A, B) + aDiag.getOffset();

	if (angle < 0)
	    angle += 360.0;

	if (angle > 360)
	    angle -= 360.0;

	cout << "Max antenna: " << maxAntenna << endl;
	cout << "Max antenna angle: " << maxAntenna * 45 << endl;
	cout << "Direction: " << direction << endl;
	cout << "Angle: " << angle << endl;
	cout << "Angle offset: " << aDiag.getOffset() << "\n----" << endl;

	return angle;
}


// see alse CalculateDirection in DetectionModule.cpp
bool SweepCircle::SignalExistence(float threshold)
{
	// linearize things
    //for (int i = 0; i < (int)sweeps.size(); i++)
        //sweeps[i].Linearize();
    //threshold = pow(10, (threshold/10)); // -75 dbm

    bool isSignal = false;

    int l = 0;
    int maxAntenna = 0;
    float power = sweeps[l].GetMax();
    for (l = 0; l < (int)sweeps.size(); l++)
    {
        float p = sweeps[l].GetMax();
        if (p > power)
        {
            power = p;
            maxAntenna = l;
        }

        if (p >= threshold)
            isSignal = true;
    }

    return isSignal;
}

SweepCircle SweepCircle::SliceByIndexes(int startInd, int endInd)
{
	SweepCircle slicedCircle;

	for (int i = 0; i < sweeps.size(); i++)
		slicedCircle.Add(sweeps[i].SliceByIndexes(startInd, endInd));

	return slicedCircle;
}

void SweepCircle::MoveUpDown(float dBm)
{
	for (int i = 0; i < sweeps.size(); i++)
		sweeps[i].MoveUpDown(dBm);	
}

/* не нужно, наверное можно выпилить */
/*
float SweepCircle::CalculateDirectionGaussianMedian(AntennaDiagram& aDiag)
{
	vector<int> maxes;
	maxes.resize(AntennaSwitch::ANTENNAS_QUANTITY); // 8 zeros
	
	vector<int> localMaxes;
	localMaxes.resize(AntennaSwitch::ANTENNAS_QUANTITY); // 8 zeros

	int threshold = -75; // dBm // -65 // -70 // -75 // AMPLITUDE_THRESHOLD

//	for (int i = 0; i < AntennaSwitch::ANTENNAS_QUANTITY; i++)
//	{
//	    float sq = 0;
//	    int j = 0;
//	    for (; j < (int) circleVector.size(); j++)
//	    {
//		sq += circleVector[j].sweeps[i].Integrate();
//	    }
//	    maxes[i] = (sq/j);
//	}
//
//	
//	int maxAntenna = distance(maxes.begin(), max_element(maxes.begin(), maxes.end()));


	for (int i = 0; i < (int) sweeps[0].sweepMax->size(); i++)
	{
	    for (int j = 0; j < AntennaSwitch::ANTENNAS_QUANTITY; j++)
	    {
			localMaxes[j] = (*sweeps[j].sweepMax)[i];
	    }
	    int maxBinAntenna = distance(localMaxes.begin(), max_element(localMaxes.begin(), localMaxes.end()));
	    if (*(max_element(localMaxes.begin(), localMaxes.end())) >= threshold)
	    	*(maxes.begin() + maxBinAntenna) += 1;
	}

	int maxAntenna = distance(maxes.begin(), max_element(maxes.begin(), maxes.end()));

	int prevAntenna = maxAntenna - 1 < 0 ? AntennaSwitch::ANTENNAS_QUANTITY - 1 : maxAntenna - 1;
	int nextAntenna = maxAntenna + 1 >= AntennaSwitch::ANTENNAS_QUANTITY ? 0 : maxAntenna + 1;
	int secondMaxAntenna;

	int direction = 1;
	if (maxes[nextAntenna] > maxes[prevAntenna])
	{
		secondMaxAntenna = nextAntenna;
	}
	else
	{
		secondMaxAntenna = prevAntenna;
		direction = -1;
	}
	
	// calculate direction for ALL of maxes above threshold  
	vector<float> calculatedProportionsVector;
	calculatedProportionsVector.reserve(sweeps[0].sweepMax->size());
	
	for (int i = 0; i < (int) sweeps[maxAntenna].sweepMax->size(); i++)
	{
	    float A = (*sweeps[maxAntenna].sweepMax)[i];
	    float B = (*sweeps[secondMaxAntenna].sweepMax)[i];
	
	    if (A >= threshold && A > B)
	    {
			//cout << A << endl;
			A = pow(10, A/10);
			B = pow(10, B/10);
			calculatedProportionsVector.push_back((A - B)/(A + B));
	    }
	}
	
	// no signal
	if (calculatedProportionsVector.size() == 0)
	{
	    cout << "No Signal" << endl;
	    return -1;
	}
	
	// calculating median value
	int median = 0;
	sort(calculatedProportionsVector.begin(), calculatedProportionsVector.end());
	for (int i = 0; i < (int) calculatedProportionsVector.size(); i++)
	{
		//cout << calculatedProportionsVector[i] << ";";
	}
	//cout << endl;
	//cout << calculatedProportionsVector.size() << endl; // wtf???!!!! pay attention
	

	if (calculatedProportionsVector.size() % 2 != 0)
	{
	    int medianIndex = (calculatedProportionsVector.size() -1) / 2;
	    median = calculatedProportionsVector[medianIndex];
	}
	else
	{
	    int medianIndex = calculatedProportionsVector.size() / 2;
	    median = (calculatedProportionsVector[medianIndex - 1] + calculatedProportionsVector[medianIndex]) / 2;
	}
	


	

	float average = accumulate(calculatedProportionsVector.begin(), calculatedProportionsVector.end(), 0) / calculatedProportionsVector.size();
	float angle = maxAntenna * 45.0 + direction * aDiag.GetDirection(average) + aDiag.getOffset();

	if (angle < 0)
	    angle += 360.0;

	if (angle > 360)
	    angle -= 360.0;

	cout << "Max antenna: " << maxAntenna << endl;
	cout << "Max antenna angle: " << maxAntenna * 45 << endl;
	cout << "Direction: " << direction << endl;
	cout << "Angle: " << angle << endl;
	cout << "Max amp: " << *max_element(localMaxes.begin(), localMaxes.end()) << " dBm" << endl;
	cout << "Angle offset: " << aDiag.getOffset() << "\n----" << endl;

	return angle;
}
*/


// самый тупой метод, считает всё из простейшей геометрии, всё перенесено в Detection module
float SweepCircle::CalculateDirection()
{
    int maxAntenna = MaxPowerDirection();

    int prevAntenna = maxAntenna - 1 < 0 ? AntennaSwitch::ANTENNAS_QUANTITY - 1 : maxAntenna - 1;
    int nextAntenna = maxAntenna + 1 >= AntennaSwitch::ANTENNAS_QUANTITY ? 0 : maxAntenna + 1;
    int secondMaxAntenna, otherAntenna;

    if (sweeps[nextAntenna].Integrate() > sweeps[prevAntenna].Integrate())
    {
        secondMaxAntenna = nextAntenna;
        otherAntenna = prevAntenna;
    }
    else
    {
        secondMaxAntenna = prevAntenna;
        otherAntenna = nextAntenna;
    }

    float point1 = sweeps[secondMaxAntenna].Integrate() / sweeps[otherAntenna].Integrate(); // отношение симметричных антенн
    float point2 = sweeps[maxAntenna].Integrate() / sweeps[secondMaxAntenna].Integrate(); // отношение близстоящих антенн
    // float point3; // отношение отношений мощностей близстоящих антенн

    float sectorCenter = maxAntenna * 45;
    float sectorWidth = 45;

    sectorWidth /= 2;
    if (secondMaxAntenna == nextAntenna)
        sectorCenter += sectorWidth;
    else
        sectorCenter -= sectorWidth;

    sectorWidth /= 2;
    if (1 - point2 > 1 - point1)
        sectorCenter += sectorWidth;
    else
        sectorCenter -= sectorWidth;

    /* well, we'll stop here */
    /*
    sectorWidth /= 2;
    if (1 - point2 > 1 - point1)
        sectorCenter += sectorWidth;
    else
        sectorCenter -= sectorWidth;
    */

    if (sectorCenter < 0)
         sectorCenter += 360;

	return sectorCenter;
}
