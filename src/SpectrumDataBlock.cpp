#include "SpectrumDataBlock.hpp"

using namespace std;
using namespace arma;

SpectrumDataBlock::SpectrumDataBlock()
{
	this->periodogram = make_shared<vector<vector<float>>>();
	this->spectrogram = make_shared<vector<vector<float>>>();
}

SpectrumDataBlock::SpectrumDataBlock(const SpectrumDataBlock& prototype)
{
	this->centerFreq = prototype.centerFreq;
	this->frequencyStep = prototype.frequencyStep;
	this->timeStep = prototype.timeStep;
	this->periodogram = prototype.periodogram;
	this->spectrogram = prototype.spectrogram;
}

void SpectrumDataBlock::PushToPeriodogram(vector<float>& v)
{
	this->periodogram->push_back(v);
}

void SpectrumDataBlock::PushToSpectrogram(vector<float>& v)
{
	this->spectrogram->push_back(v);
}

int SpectrumDataBlock::GetTimeSize()
{
	return this->periodogram->size();
}

int SpectrumDataBlock::GetFreqSize()
{
	return (this->periodogram->at(0)).size();
}

void SpectrumDataBlock::ToOutput()
{
	for (int i = 0; i < this->periodogram->size(); i++)
		MathModule::VecToOutput((periodogram->at(i)));
	cerr << endl;
}

void SpectrumDataBlock::ToOutputSpectr()
{
	cout << "Here!Spectr" << endl;
	for (int i = 0; i < this->spectrogram->size(); i++)
	{
		MathModule::VecToOutput((spectrogram->at(i)));
	}
	cerr << endl;
}

vector<float> SpectrumDataBlock::FreqProjection()
{
	vector<float> freqProjection;
	freqProjection = this->periodogram->at(0);
	for (int i = 1; i < this->periodogram->size(); i++)
	{
		for (int n = 0; n < freqProjection.size(); n++)
		{
			if (freqProjection[n] < (this->periodogram->at(i)).at(n))
				freqProjection[n] = (this->periodogram->at(i)).at(n);
		}
	}

	return freqProjection;
}

vector<float> SpectrumDataBlock::FreqSpectrumProjection()
{
	vector<float> freqProjection;
	freqProjection = this->spectrogram->at(0);
	for (int i = 1; i < this->spectrogram->size(); i++)
	{
		for (int n = 0; n < freqProjection.size(); n++)
		{
			if (freqProjection[n] < (this->spectrogram->at(i)).at(n))
				freqProjection[n] = (this->spectrogram->at(i)).at(n);
		}
	}

	return freqProjection;
}

vector<float> SpectrumDataBlock::TimeProjection()
{
	vector<float> timeProjection;
	timeProjection.resize(this->periodogram->size());
	for (int i = 0; i < this->periodogram->size(); i++)
	{
		float value = (this->periodogram->at(i)).at(0);
		for (int n = 0; n < (this->periodogram->at(i)).size(); n++)
		{
			if (value < (this->periodogram->at(i)).at(n))
				value = (this->periodogram->at(i)).at(n);
		}
		timeProjection[i] = value;
	}

	return timeProjection;
}

vector<float> SpectrumDataBlock::TimeSpectrumProjection()
{
	vector<float> timeProjection;
	timeProjection.resize(this->spectrogram->size());
	for (int i = 0; i < this->spectrogram->size(); i++)
	{
		float value = (this->spectrogram->at(i)).at(0);
		for (int n = 0; n < (this->spectrogram->at(i)).size(); n++)
		{
			if (value < (this->spectrogram->at(i)).at(n))
				value = (this->spectrogram->at(i)).at(n);
		}
		timeProjection[i] = value;
	}

	return timeProjection;
}

void SpectrumDataBlock::CalculateEntropy(vector<float>& entropyVec)
{
	entropyVec.resize(this->spectrogram->size(), 0);
	for (int i = 0; i < this->spectrogram->size(); i++)
	{
		for (int n = 0; n < (this->spectrogram->at(i)).size(); n++)
		{
			float value = (this->spectrogram->at(i)).at(n);
			entropyVec[i] += value * log2f(value);  // log() is also ok, but log2() works a bit better
		}
	}
}


void SpectrumDataBlock::GetTimeIndexSlice(vector<vector<float>>& timeSlicedSpectrogram, vector<vector<float>>& inSpectrogram, int startInd, int endInd)
{
	int timeIndexDelta = endInd - startInd;
	if (startInd >= endInd || endInd > inSpectrogram.size())
		return;

	for (int i = startInd; i < endInd; i++)
		timeSlicedSpectrogram.push_back(inSpectrogram[i]);
}

void SpectrumDataBlock::GetFreqIndexSlice(vector<vector<float>>& freqSlicedSpectrogram, vector<vector<float>>& inSpectrogram, int startInd, int endInd)
{
	int freqIndexDelta = endInd - startInd;
	if (startInd >= endInd || endInd > inSpectrogram[0].size())
		return;

	vector<float>::iterator beginIter;
	vector<float> singleFreqSliceVector(freqIndexDelta);
	for (int i = 0; i < inSpectrogram.size(); i++)
	{
		copy (inSpectrogram[i].begin() + startInd,
			 inSpectrogram[i].begin() + endInd,
			 singleFreqSliceVector.begin());
		freqSlicedSpectrogram.push_back(singleFreqSliceVector);
	}
}

/* Этот метод, который выделяет пики - это просто трэш. Нужно переписать   */
vector<SpectrumDataBlock> SpectrumDataBlock::GetThresholdSlicedDatablocks(float threshold)
{
	vector<SpectrumDataBlock> slicedDatablocks;

	vector<float> freqProjection = FreqSpectrumProjection();
	MathModule::SmoothVector(freqProjection, 41); // 41 взяли на глаз
	//MathModule::VecToOutput(freqProjection);
	vector<int> crossingIndexes;
	crossingIndexes.reserve(2);
	float prev = freqProjection[1];
	for (int i = 1; i < freqProjection.size(); i++)
	{

		if (crossingIndexes.size() == 0)
		{
			if (prev <= threshold && freqProjection[i] > threshold)
			{
				crossingIndexes.push_back(i - 1);
				//cout <<  i - 1 <<  endl;
			}
		}
		else
		{
			if (prev > threshold && freqProjection[i] < threshold)
			{
				crossingIndexes.push_back(i);
				//cout <<  i <<  endl;
			}
		}
		prev = freqProjection[i];

		/* Костыль!!!!!! */ /*убираем все промежутки, длительностью меньше 10 индексов - это не сигнал*/
		if ((crossingIndexes.size() == 2) && (crossingIndexes[1] - crossingIndexes[0] < 7))
			crossingIndexes.clear();

		if (crossingIndexes.size() == 2)
		{
			SpectrumDataBlock freqSlicedBlock;
			freqSlicedBlock.frequencyStep = this->frequencyStep;
			freqSlicedBlock.timeStep = this->timeStep;

			/* Slice by freq */
			vector<vector<float>>& freqSlicedBlocklink = *(freqSlicedBlock.spectrogram);
			GetFreqIndexSlice(freqSlicedBlocklink, *spectrogram, 
				crossingIndexes[0], crossingIndexes[1]);
			freqSlicedBlock.centerFreq = this->centerFreq + (frequencyStep * crossingIndexes[0]) 
					- (frequencyStep * (freqProjection.size() - crossingIndexes[1]));

			// output works
			/* 
			for (int i = 0; i < freqSlicedBlocklink.size(); i++)
			{
				MathModule::VecToOutput(freqSlicedBlocklink[i]);
				cerr << endl;
			}
			*/
						
			slicedDatablocks.push_back(freqSlicedBlock);
			crossingIndexes.clear();
		}
	}

	/* срез по времени */
	vector<SpectrumDataBlock> timeSlicedDatablocks;
	crossingIndexes.clear();
	for (int i = 0; i < slicedDatablocks.size(); i++)
	{
		SpectrumDataBlock& currentDatablock = slicedDatablocks[i];
		vector<float> timeProjection = currentDatablock.TimeSpectrumProjection();
		float prev = timeProjection[1];
		for (int i = 1; i < timeProjection.size(); i++)
		{
			if (crossingIndexes.size() == 0)
			{
				if (prev <= threshold && timeProjection[i] > threshold)
					crossingIndexes.push_back(i - 1);
			}
			else
			{
				if (prev > threshold && timeProjection[i] < threshold)
					crossingIndexes.push_back(i);
			}
			prev = timeProjection[i];

			/* Костыль!!!!!! */		/*убираем все промежутки, длительностью меньше 10 индексов - это не сигнал (скорее всего) */
			if ((crossingIndexes.size() == 2) && (crossingIndexes[1] - crossingIndexes[0] < 7))
				crossingIndexes.clear();

			if (crossingIndexes.size() == 2)
			{
				SpectrumDataBlock timeSlicedBlock;
				//vector<vector<float>> slicedSpectrogramTimeVector;
				timeSlicedBlock.frequencyStep = currentDatablock.frequencyStep;
				timeSlicedBlock.timeStep = currentDatablock.timeStep;
				timeSlicedBlock.centerFreq = currentDatablock.centerFreq;
	
				/* Slice by time */
				vector<vector<float>>& timeSlicedBlocklink = *(timeSlicedBlock.spectrogram);
				GetTimeIndexSlice(*(timeSlicedBlock.spectrogram), *(currentDatablock.spectrogram), crossingIndexes[0], crossingIndexes[1]);
	
							
				timeSlicedDatablocks.push_back(timeSlicedBlock);
				crossingIndexes.clear();
			}
		}	
	}

	return timeSlicedDatablocks;
}