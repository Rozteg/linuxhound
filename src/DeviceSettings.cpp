#include "DeviceSettings.hpp"

using namespace std;

DeviceSettings::DeviceSettings()
{
    detector = MIN_AND_MAX;
    scale = LOG;

    center = 10e6f; // 10 MHz
    span = 1e6f; // 1 MHz

    ref = 0;
    atten = ATTEN_AUTO;

    gain = GAIN_AUTO;
    resolutionBandwidth = 10.3e3;
    videoBandwidth = 10.3e3;
    sweepTime = 0.001; // 1ms
    rbwShape = NUTTALL;
    rejection = NO_SPUR_REJECT;

    procUnits = PROC_LOG;
}

string DeviceSettings::ToString()
{
    ostringstream stringStream;
    stringStream << "Center:"       << center
                 << ",Span:"        << span 
                 << ",Det:"         << detector 
                 << ",Scale:"       << scale 
                 << ",Ref:"         << ref 
                 << ",Atten:"       << atten
                 << ",Gain:"        << gain
                 << ",RBW:"         << resolutionBandwidth
                 << ",VBW:"         << videoBandwidth
                 << ",SweepTime:"   << sweepTime
                 << ",RBWShape:"    << rbwShape
                 << ",Rejection:"   << rejection
                 << ",ProcUnits:"   << procUnits;


    return stringStream.str();
}

// getters and setters
bbDetector DeviceSettings::getDetector() { return detector; }
void DeviceSettings::setDetector(bbDetector detector) { this->detector = detector; }
////

bbScale DeviceSettings::getScale() { return scale; }
void DeviceSettings::setScale(bbScale scale) { this->scale = scale; }
////

double DeviceSettings::getCenter() { return center; }
void DeviceSettings::setCenter(double center) { this->center = center; }
////

double DeviceSettings::getSpan() { return span; }
void DeviceSettings::setSpan(double span) { this->span = span; }
////

double DeviceSettings::getRef() { return ref; }
void DeviceSettings::setRef(double ref) { this->ref = ref; }
////

double DeviceSettings::getAtten()
{
    if (atten < 0)
        return (double)atten;
    else
        return (double)(10.0 * atten);
}
void DeviceSettings::setAtten(bbAtten atten) { this->atten = atten; }
////

int DeviceSettings::getGain() { return (int)gain; }
void DeviceSettings::setGain(bbGain gain) { this->gain = gain; }
////

double DeviceSettings::getRBW() { return resolutionBandwidth; }
void DeviceSettings::setRBW(double resolutionBandwidth) { this->resolutionBandwidth = resolutionBandwidth; }
////

double DeviceSettings::getVBW() { return videoBandwidth; }
void DeviceSettings::setVBW(double videoBandwidth) { this->videoBandwidth = videoBandwidth; }
////

double DeviceSettings::getSweepTime() { return sweepTime; }
void DeviceSettings::setSweepTime(double sweepTime) { this->sweepTime = sweepTime; }
////

bbRBWShape DeviceSettings::getRBWShape() { return rbwShape; }
void DeviceSettings::setRBWShape(bbRBWShape rbwShape) { this->rbwShape = rbwShape; }
////

bbReject DeviceSettings::getRejection() { return rejection; }
void DeviceSettings::setRejection(bbReject rejection) { this->rejection = rejection; }
////

bbProcUnits DeviceSettings::getProcUnits() { return procUnits; }
void DeviceSettings::setProcUnits(bbProcUnits procUnits) { this->procUnits = procUnits; }
////
