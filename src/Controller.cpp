#include "Controller.hpp"

#include <chrono> // для измерний времени
using namespace std::chrono; 
using namespace std;

// Мерять время вот так!
//auto start = high_resolution_clock::now();
//auto stop = high_resolution_clock::now();
//auto duration = duration_cast<microseconds>(stop - start);
//cout << "push time : " << duration1.count() / 1.0e6 << endl;

vector<SweepCircle> Controller::initialSweepCircles;

void Controller::CheckInitDevice()
{
	if (SignalHound::id < 0 || SignalHound::status < 0)
	{
		// нужно добавить сообщение об ошибке
		SignalHound::CloseDevice();
		SignalHound::OpenDevice();
		SignalHound::ConfigureDevice();
		SignalHound::InitDeviceSweep();
	}
}

void Controller::CheckInitDeviceIQ()
{
	if (SignalHound::id < 0 || SignalHound::status < 0)
	{
		// нужно добавить сообщение об ошибке
		SignalHound::CloseDevice();
		SignalHound::OpenDevice();
		// перенесено ниже //SignalHound::ConfigureDeviceIQ(SignalHound::deviceSettings);
	}
	SignalHound::ConfigureDeviceIQ(SignalHound::deviceSettings);
}

void Controller::CheckInitAntennaSwitch()
{
	//return; // for debug
	if (AntennaSwitch::ftStatus != FT_OK || AntennaSwitch::ftdiDevice == NULL)
	{
		// нужно добавить сообщение об ошибке
		AntennaSwitch::CloseDevice();
		AntennaSwitch::OpenDevice();
		AntennaSwitch::SwitchAntenna(AntennaSwitch::initialAntenna);
	}
}

void Controller::Sweep()
{
	CheckInitDevice();
	CheckInitAntennaSwitch();
	SignalHound::TakeSweep();
}

// здесь мы делаем круговой свип первичной радиоэлектронной обстановки
void Controller::GetInitialCircleSweeps()
{
	cout << "Initial scanning..." << endl;
	// Максимальное время наблюдения с одной антенны - 100 миллисекунд (из настроек Signal Hound)
	int circles = 5;

	for (int i = 0; i < ConfigParser::settingsVec.size(); i++)
	{
		SignalHound::ConfigureDevice(ConfigParser::settingsVec[i]);
        SignalHound::InitDeviceSweep();

		vector<SweepCircle> sweepCircleVec;
		sweepCircleVec.reserve(circles);
		for (unsigned int i = 0; i < circles; i++)
			sweepCircleVec.push_back(SweepCircle());
	
		CheckInitDevice();
		CheckInitAntennaSwitch();
	
		//auto start = high_resolution_clock::now(); 
		for (unsigned int i = 0; i < circles; i++)
		{
			AntennaSwitch::SwitchAntenna(AntennaSwitch::Antenna::ONE);
			for (int n = 0; n < AntennaSwitch::ANTENNAS_QUANTITY; n++)
			{
				SignalHound::TakeSweep();
				sweepCircleVec[i].Add(SignalHound::lastSweep);
				AntennaSwitch::NextAntenna();
			}
		}
	
		// Делаем MaxHold по всем кругам
		for (unsigned int i = 1; i < circles; i++)
		    sweepCircleVec[0].Merge(sweepCircleVec[i]);
	
		sweepCircleVec[0].Smoothen();
		sweepCircleVec[0].MoveUpDown(10); // приподнимаем нашу огибающую на сколько-то dBm (лучше на 5 минимум) // ВАЖНО! здесь мы задаём уровень нуля
		//cerr << sweepCircleVec[0].ToString(); // log
		Controller::initialSweepCircles.push_back(sweepCircleVec[0]);
		cout << "Initial scan " << i << " Done." << endl;
	}
	cout << "Initial scanning Done." << endl;

	//exit(0);
}

double Controller::CircleSweep(unsigned int circles)
{
	vector<SweepCircle> sweepCircleVec;
	sweepCircleVec.reserve(circles);
	for (unsigned int i = 0; i < circles; i++)
		sweepCircleVec.push_back(SweepCircle());

	CheckInitDevice();
	CheckInitAntennaSwitch();

	//auto start = high_resolution_clock::now(); 
	for (unsigned int i = 0; i < circles; i++)
	{
		AntennaSwitch::SwitchAntenna(AntennaSwitch::Antenna::ONE);
		for (int n = 0; n < AntennaSwitch::ANTENNAS_QUANTITY; n++)
		{
			SignalHound::TakeSweep();
			sweepCircleVec[i].Add(SignalHound::lastSweep);
			AntennaSwitch::NextAntenna();
		}
	}


	//auto stop = high_resolution_clock::now(); 
	//auto duration = duration_cast<microseconds>(stop - start);
	//cout << "CircleSweep Time: " << duration.count() / 1.0e6 << endl;

	// log
	//cerr << sweepCircleVec[0].ToString();

	// may be we should forget about it
	// denoising all
	//for (unsigned int j = 0; j < sweepCircleVec.size(); j++)
	//{
	//    for (unsigned int i = 0; i < sweepCircleVec[j].sweeps.size(); i++)
	//    MathModule::WaveletDenoise((*sweepCircleVec[j].sweeps[i].sweepMax));
	//}

	//for (unsigned int i = 1; i < circles; i++)
	//    sweepCircleVec[0].Merge(sweepCircleVec[i]);

	//sweepCircleVec[0].MergeAverage(sweepCircleVec);	

	//cerr << sweepCircleVec[0].ToString();

	double angle = DetectionModule::CalculateDirection(sweepCircleVec[0]); // ЗДЕСЬ вычисляем направление
	cerr << angle << endl;

	//cerr << DetectionModule::CalculateDirectionStatistically(sweepCircleVec[0]) << endl;

	//cout << sweepCircleVec[0].MaxesToString() << "\n"; 

	cout << sweepCircleVec[0].MaxesToString() << "\n";
	//cerr << sweepCircleVec[0].MaxesToString() << "\n";


	// функции ниже не нужны
	//return sweepCircleVec[0].CalculateDirectionGaussianMedian(ADiag);
	//return sweepCircleVec[0].CalculateDirectionGaussian(ADiag);
	//return sweepCircleVec[0].CalculateDirectionGaussianAverage(ADiag, sweepCircleVec);

	return angle;
	//return -1;
}

void Controller::CircleSweepMultipleDetect(int indexOfSettings)
{
	int circles = 1; // сколько кругов будем сканировать
	vector<SweepCircle> sweepCircleVec;
	sweepCircleVec.reserve(circles);
	for (unsigned int i = 0; i < circles; i++)
		sweepCircleVec.push_back(SweepCircle());

	CheckInitDevice();
	CheckInitAntennaSwitch();

	for (unsigned int i = 0; i < circles; i++)
	{
		AntennaSwitch::SwitchAntenna(AntennaSwitch::Antenna::ONE);
		for (int n = 0; n < AntennaSwitch::ANTENNAS_QUANTITY; n++)
		{
			SignalHound::TakeSweep();
			sweepCircleVec[i].Add(SignalHound::lastSweep);
			AntennaSwitch::NextAntenna();
		}
	}

	for (unsigned int i = 1; i < circles; i++)
	    sweepCircleVec[0].Merge(sweepCircleVec[i]);

	/* вот на этом месте sweepCircleVec[0] можно дополнительно сгладить (скользящим максимумом, например), для лучшего отсечения отрезков сигнала*/

	vector<SweepCircle> slicedCircles; // Сюда мы складываем "кусочки" кругового свипа, где обнаружим сигнал
	DetectionModule::MultipleDetect(sweepCircleVec[0], initialSweepCircles[indexOfSettings], slicedCircles);

	//cerr << sweepCircleVec[0].ToString();


	/* вот здесь мы выводим углы сигналов, повторяющиеся углы можно не выводить */ 
	cout << "found signals: " << slicedCircles.size() << endl;
	cout << "destinations: " << endl;
	for (int i = 0; i < slicedCircles.size(); ++i)
	{
		double angle = DetectionModule::CalculateDirection(slicedCircles[i]);
		cerr << angle << endl;
	}
	cout << "----------------" << endl;
}


void Controller::IQTraceScan(int antennaNum)
{
	//vector<IQTrace> circleIQ;
	//circleIQ.reserve(SweepCircle::ANTENNAS_QUANTITY);

	int cycles = 4;
	
	IQTrace antennaTrace(SignalHound::sampleRate, SignalHound::blockLength * cycles,
			 SignalHound::bandwidth, SignalHound::deviceSettings.getCenter());
	vector<float>::iterator traceIter = antennaTrace.buffer->begin();

	CheckInitDeviceIQ();
	CheckInitAntennaSwitch();

	//AntennaSwitch::SwitchAntenna(AntennaSwitch::NumToAntenna(antennaNum));
	AntennaSwitch::SwitchAntenna(AntennaSwitch::Antenna::ONE);
	// For every antenna // SweepCircle::ANTENNAS_QUANTITY

	//SweepCircle::ANTENNAS_QUANTITY
	for (int i = 0; i < 1; i++)
	{
		// make n = cycles measurments

		// IQTrace multiplies blockLength by 2 on constructor call, be aware

		//IQTrace antennaTrace(SignalHound::sampleRate, SignalHound::blockLength * cycles,
		//	 SignalHound::bandwidth, SignalHound::deviceSettings.getCenter());
		//vector<float>::iterator traceIter = antennaTrace.buffer->begin();
	
		//SignalHound::FetchRaw(&(*traceIter)); // make buffer empty
	
		for (int n = 0; n < cycles; n++)
		{
			SignalHound::FetchRaw(&(*traceIter));
	
			/* Simulation */
			//vector<float> signal;
	
			//MathModule::SinWaveGen(signal, 13107.175e-6, 6e6, 40.0e6);
			//copy(signal.begin(), signal.end(), traceIter);
			/* end of simulation */

			// shift insert pointer
			traceIter += antennaTrace.getDoubleBlockLen() / cycles;
		}
	}

// Debug. Be aware of tons of data.
/*
	for (unsigned int i = 0 ; i < circleIQ.size(); i++)
	{
		cerr << i << ";" << circleIQ[i].ToString()
		<< endl;
	}
*/


	//for (unsigned int i = 0 ; i < circleIQ.size(); i++)
	//{
	//		circleIQ[i].DetectCamera();
	//}

	//DetectionModule::DetectCamera(antennaTrace);
	/* можно использовать как верхнюю, так и нижнюю функцию */
	DetectionModule::SimpleDetectCamera(antennaTrace);
}


double Controller::Scan()
{
	SweepCircle sweepCircle; 

	CheckInitDevice();
	CheckInitAntennaSwitch();

	// Собираем свипы в режиме сканирования
	AntennaSwitch::SwitchAntenna(AntennaSwitch::Antenna::ONE);
	for (int n = 0; n < AntennaSwitch::ANTENNAS_QUANTITY; n++)
	{
		SignalHound::TakeSweep();
		sweepCircle.Add(SignalHound::lastSweep);
		AntennaSwitch::NextAntenna();
	}

	// Ищем частоту с максимальной энергией и антенну
	/* -70dBm - пороговое значение */
	if (sweepCircle.SignalExistence(-70))
	{
		int direction = sweepCircle.MaxPowerDirection();
		double cenerFreq = sweepCircle.sweeps[direction].GetMaxFreq();
	
		cout << "Max center freq: " << cenerFreq << endl;
		cout << "Max direction: " << direction << endl;

		// сохраняем оригинальные настройки и переключамся
		// на режим сбора IQ компонент с переключением частоты
		DeviceSettings originalSettings = SignalHound::deviceSettings;
		DeviceSettings iQScanSettings = originalSettings;

		/// цикл сканирования целей
		SignalHound::ConfigureDeviceIQ(iQScanSettings);
		ContiguousTimeBlock ctb;
		for (int n = 0; n < 4; n++) // Смещаемся по времени 4 раза
		{
			ContiguousFreqBlock cfb;
			float overlapping = 0.5; // внахлёст 50%
			cfb.overlapping = overlapping;
			for (int i = 0; i < 4; i++) // Смещаемся по частоте 4 раза
			{
				iQScanSettings.setCenter(cenerFreq + i * (-BB_IQ_SAMPLES_PER_SEC * overlapping)
										 - BB_IQ_SAMPLES_PER_SEC); // полоса в 80MHz

				SignalHound::ConfigureDeviceIQ(iQScanSettings);
				AntennaSwitch::SwitchAntenna(AntennaSwitch::NumToAntenna(direction));

				IQPeriodogramBlock iqPer;
				iqPer.iqtrace.Init(SignalHound::sampleRate, SignalHound::blockLength,
				SignalHound::bandwidth, SignalHound::deviceSettings.getCenter());
				vector<float>::iterator traceIter = iqPer.iqtrace.getBufferStartIter();

				SignalHound::FetchRaw(&(*traceIter));
				//cout << "Got IQ trace" << endl;

				//auto start1 = high_resolution_clock::now(); 
				cfb.PushToBlock(iqPer);
				//auto stop1 = high_resolution_clock::now(); 
				//auto duration1 = duration_cast<microseconds>(stop1 - start1);
				//cout << "push time : " << duration1.count() / 1.0e6 << endl;


			}

			ctb.PushToBlock(cfb);
		}

		//ctb.ToSpectrum();
		//ctb.ToOutput();
		//vector<float> v = ctb.FreqProjection();
		//MathModule::VecToOutput(v);
		//cout << "freqProjection done!" << endl;
		Controller::AnalyseTimeBlock(ctb); // вот здесь определяем, является ли сигнал ППРЧ

		//exit(0);

		// Переключаемся обратно в режим сканирования
		SignalHound::ConfigureDevice(originalSettings);
	}


	return -2;
}

/* Эту функцию нужно перенести в другой модуль */
void Controller::AnalyseTimeBlock(ContiguousTimeBlock& timeblock)
{
	timeblock.ToSpectrum();
	vector<float> timeProjection = timeblock.TimeProjection();
	vector<float> freqProjection = timeblock.FreqProjection();

	// smoothing
	int windowWidth = 41; // Ширина окна сглаживания в отсчётах // подбирается эмпирическим путём. 21, вроде норм
	MathModule::SmoothVector(freqProjection, windowWidth);
	//MathModule::VecToOutput(freqProjection);

	float epsilon = 0.001;
	float koef = 1.7; // волшебная цифра, должна быть больше 1. отсекает уровень шума от пиков (1 оставит моду на уровне шума)
	float mode = MathModule::CalculateFloatMode(freqProjection, epsilon) * koef; // умножаем моду на коэффициент, мода примерно показывает уровень шума
	// чтобы заменить моду на любую другую линию отсечения, просто замените CalculateFloatMode на нужную вам функцию

	vector<SpectrumDataBlock> slicedDataBlocks;
	slicedDataBlocks = timeblock.GetThresholdSlicedDatablocks(mode);

	/* output */ 
	cout << endl;
	cout << "outputting" << endl;
	cout << slicedDataBlocks.size() << endl;
	
	for (int i = 0; i < slicedDataBlocks.size(); i++)
	{
		slicedDataBlocks[i].ToOutputSpectr();
		cout << "peak number: " << i << endl;
	} 
	/* end output */

	/* processing */
	for (int i = 0; i < slicedDataBlocks.size(); i++)
		Controller::ProcessSlicedSignal(slicedDataBlocks[i]);

	/* analyse signal */
}

/* эту тоже перенести в другой модуль */
void Controller::ProcessSlicedSignal(SpectrumDataBlock dataBlock)
{
	//float duration = (dataBlock.GetTimeSize() + 1) * FFT_WINDOW_WIDTH_HALF_INTERVAL * BB_IQ_TIME_DELTA;
	float duration = (dataBlock.GetTimeSize() + 1) * FFT_WINDOW_WIDTH_HALF_INTERVAL;

	// (duration >= BB_IQ_BUFFER_SIZE * 0.9) // примерно так
	// проверка на то, является ли сигнал постоянно присутствующим на всём протяжении снятого IQ участка
	if (duration == BB_IQ_BUFFER_SIZE)
	{
		cout << "Signal is persistent, probably, camera" << endl;
		return;
	}

	/* Посчитать энтропию Шеннона для каждого fft сигнала */
	vector<float> signalEntropy;
	dataBlock.CalculateEntropy(signalEntropy);

	/* переход от преамбулы к передаче находится где-то на уровне максимума энтропии  */
	/* утверждение выше действительно только для 2 пультов, у futaba t8 fg super энтропия выглядит так -  /\_________/\ */
	/* то есть, длинный ровный участок между пиками, локальный минимум может оказаться до первого пика, это проблема */
	/* предлагаемое решение - искать в первой трети сигнала участки, в которых дисперсия расстояния между пиками меньше единицы */
	/* нашли хотя бы 2 последовтельных fft с такой штукой - всё, это ппрч сигнал*/
	float entropyMax = *max_element(signalEntropy.begin(), signalEntropy.end());
	float entropyLowerBorder = entropyMax * 0.95; // где-то в пределах 5% от максимума ищем, 5% взято из эмпирического опыта (на глаз)

	int endOfinterval = 0;
	int scanTo = (int)(signalEntropy.size() / 2); // интервал должен быть в первой трети сигнала (на глаз)
	for (int i = 1; i <= scanTo ; i++)
	{
		// переход от преамбулы к передаче является локальным максимумом (по крайней мере, для двух пультов это так, но не для futaba t8gsuper)
		if (signalEntropy[i] > signalEntropy[i+1] &&
			signalEntropy[i] > signalEntropy[i-1] &&
			signalEntropy[i] >= entropyLowerBorder)
		{
			endOfinterval = i;
			break;
		}
	}
	if (endOfinterval == 0)
	{
		cout << "no end of interval was found" << endl;
		return;
	}

	// теперь идем обратно - ищем локальный минимум, он будет являться одним из самых стабильных fft преамбулы
	int beginOfInterval = 0;
	for (int i = endOfinterval - 1; i > 0; i--)
	{
		if (signalEntropy[i] < signalEntropy[i+1] &&
			signalEntropy[i] < signalEntropy[i-1])
		{
			beginOfInterval = i;
			break;
		}
	}
	if (beginOfInterval == 0)
	{
		cout << "no begin of interval was found" << endl;
		return;
	}

	int lengthOfInterval = endOfinterval - (beginOfInterval - 1);
	//float timeDurationOfInterval = (lengthOfInterval + 1) * FFT_WINDOW_WIDTH_HALF_INTERVAL * BB_IQ_TIME_DELTA;

	vector<float>& adjustmentFFT = dataBlock.spectrogram->at(beginOfInterval);
	vector<int> peakInds;
	Util::findPeaks(adjustmentFFT, peakInds);

	vector<int> peakDistances(peakInds.size() - 1);
	for (int i = 0; i < peakInds.size() - 1; i++)
		peakDistances[i] = peakInds[i+1] - peakInds[i];

	cout << "=====Slice=====" << endl;
	cout << "Freq: " << dataBlock.centerFreq << endl;
	cout << "adjust len: " << lengthOfInterval << endl;
	cout << "peaks: " << peakInds.size() << endl; // количество пиков 
	cout << "peaks distances: "; // расстояние между пиками в бинах (для одного пульта расстояние в 12-13 бинов, для другого в 3-4 бина)
	// можно, вообще, складывать расстояние между пиками в массив и считать от него дисперсию(вариацию),
	// если дисперсия меньше 1 то это точно гребёнка участка подстройки ППРЧ сигнала
	for (int i = 0; i < peakDistances.size(); i++)
		cout << " " << peakDistances[i];
	cout << endl;
	for (int i = 0; i < peakDistances.size(); i++)
		cout << " " << peakDistances[i] * dataBlock.frequencyStep;
	cout << endl;

	cout << "=====Slice=====" << endl;
}