#include "AntennaDiagram.hpp"

using namespace std;
using namespace arma;

/* empty constructor not usable */
AntennaDiagram::AntennaDiagram()
{
    vector<double> proportionsVector;
    this->pVec = proportionsVector;
    this->delta = 0;
    this->offset = 0;
}

AntennaDiagram::AntennaDiagram(double sigma, double delta, double offset)
{
    this->pVec = AntennaDiagram::AntennaDiagramTable(delta, sigma);
    this->delta = delta;
    this->offset = offset;
}

double AntennaDiagram::GetDirectionStep(double calcValue)
{
    vector<double> vectorOfDeltas;
    vectorOfDeltas.reserve(pVec.size());
    for(int i = 0; i < (int) pVec.size(); i++)
	   vectorOfDeltas.push_back(abs(calcValue - pVec[i]));

    return ((int) distance(vectorOfDeltas.begin(), min_element(vectorOfDeltas.begin(), vectorOfDeltas.end())));

/*
    cout << calcValue << "\n";
    cout << angle << "\n";
    for(int i = 0; i < (int) pVec.size(); i++)
        cout << pVec[i] << " ";

    cout << "\n";

    for(int i = 0; i < (int) vectorOfDeltas.size(); i++)
        cout << vectorOfDeltas[i] << " ";

    cout << "\n";
    cout << "----\n";
*/

}

double AntennaDiagram::GetDirection(double a, double b)
{
    double calcValue =  AntennaDiagram::AntennaDiffDivSum(a, b);
    return (GetDirectionStep(calcValue) * delta);
}

double AntennaDiagram::getOffset()
{
    return offset;
}

double AntennaDiagram::AntennaDiffDivSum(double a, double b)
{
    return (a - b) / (a + b);
}

double AntennaDiagram::AntennaMaxDivMin(double a, double b)
{
    return (a / b);
}

vector<double> AntennaDiagram::AntennaDiagramTable(double step, double sigma)
{
    vector<double> X;
    for (double i = -45; i <= 45; i += step)
        X.push_back(i);

    vec gaussian = normpdf(vec(X), 0, sigma);
    double max = gaussian.max();

    vector<double> g1;
    vector<double> g2;
    vector<double> g;
    vector<double> gsimple;

    bool afterMax = false;
    for(int i = 0; i < (int) gaussian.size(); i++)
    {
       if (gaussian[i] == max)
           afterMax = true;
    
       if (afterMax)
           g1.push_back(gaussian[i]);
    }

    for(int i = g1.size() - 1; i >= 0; i--)
        g2.push_back(g1[i]);

    int n = 0;
    for (n = 0; g2[n] <= g1[n]; n++) { }

    for (int i = 0; i < n; i++)
    {
        g.push_back(AntennaDiagram::AntennaDiffDivSum(g1[i], g2[i]));
        gsimple.push_back(AntennaDiagram::AntennaMaxDivMin(g1[i], g2[i]));
    }

    // debug
    for (int i = 0; i < (int) g.size(); i++)
       cout << g[i] << ";";
    cout << endl;

    for (int i = 0; i < (int)  gsimple.size(); i++)
       cout << gsimple[i] << ";";
    cout << endl;

    return g;
    //return gsimple;
}