#include "TCPServer.h"

using namespace std;

int TCPServer::sockfd;
struct sockaddr_in TCPServer::serverAddress;
fd_set TCPServer::readset;
int TCPServer::reuse;
struct timeval TCPServer::timeout;
set<int> TCPServer::connected;
pthread_t TCPServer::serverThread;
mutex TCPServer::connectedMutex;

void* TCPServer::Task(void* args)
{
	// loop looking for connections / data to handle
	pthread_detach(pthread_self());

	int bytes_read;
	char buf[1024];

	timeout.tv_sec = 3;
	timeout.tv_usec = 0;

	connected.clear();
	while(1)
	{
		// Заполняем множество сокетов
		FD_ZERO(&readset);
		FD_SET(sockfd, &readset);

		connectedMutex.lock();
		for(set<int>::iterator it = connected.begin(); it != connected.end(); it++)
			FD_SET(*it, &readset);
		connectedMutex.unlock();

		// Задаём таймаут

		// Ждём события в одном из сокетов
		connectedMutex.lock();
		int mx = max(sockfd, *max_element(connected.begin(), connected.end()));
		if(select(mx+1, &readset, NULL, NULL, &timeout) < 0)
		{
			perror("select");
			exit(3);
		}
		connectedMutex.unlock();


		// Определяем тип события и выполняем соответствующие действия
		if(FD_ISSET(sockfd, &readset))
		{
			// Поступил новый запрос на соединение, используем accept
			int sock = accept(sockfd, NULL, NULL);
			if(sock < 0)
			{
				perror("accept");
				exit(3);
			}
			
			fcntl(sock, F_SETFL, SOCK_NONBLOCK);

			connectedMutex.lock();
			connected.insert(sock);
			connectedMutex.unlock();
		}

		connectedMutex.lock();
		for(set<int>::iterator it = connected.begin(); it != connected.end(); it++)
		{
			if(FD_ISSET(*it, &readset))
			{
				// Поступили данные от клиента, читаем их
				bytes_read = recv(*it, buf, 1024, 0);

				if(bytes_read <= 0)
				{
					// Соединение разорвано, удаляем сокет из множества
					close(*it);
					connected.erase(*it);
				}
			}
		}
		connectedMutex.unlock();
	}

}

void TCPServer::Setup(int port)
{
	reuse = 1;
	sockfd = socket(AF_INET,SOCK_STREAM, 0);
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int));
	memset(&serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family=AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(port);
	bind(sockfd, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
	listen(sockfd, 5);
	pthread_create(&serverThread, NULL, &Task, NULL);
}

void TCPServer::Send(string msg)
{
	connectedMutex.lock();
	for (set<int>::iterator it = connected.begin(); it != connected.end(); ++it)
		send(*it, msg.c_str(), msg.length(), 0);
	connectedMutex.unlock();
}

void TCPServer::Detach()
{
	close(sockfd);
	for (set<int>::iterator it = connected.begin(); it != connected.end(); ++it)
		close(*it);
	connected.clear();
}
