#include "IQTrace.hpp"

using namespace std;
using namespace arma;

IQTrace::IQTrace()
{
    buffer = make_shared<vector<float>>();
}

IQTrace::IQTrace(IQTrace& prototype)
{
    this->sampleRate = prototype.sampleRate;
    this->blockLength = prototype.blockLength;
    this->bandwidth = prototype.bandwidth;
    this->centerFreq = prototype.centerFreq;
    this->buffer = prototype.buffer;
}

IQTrace::IQTrace(int sampleRate, int blockLength, double bandwidth, double centerFreq)
{
    Init(sampleRate, blockLength, bandwidth, centerFreq);
}

void IQTrace::Init(int sampleRate, int blockLength, double bandwidth, double centerFreq)
{
    this->sampleRate = sampleRate;
    this->blockLength = blockLength;
    this->bandwidth = bandwidth;
    this->centerFreq = centerFreq;
    buffer = make_unique<vector<float>>(blockLength * 2);   
}

double IQTrace::GetTime(int i)
{
    if (i >= 0 && i < (int) (buffer->size() / 2))
        return (i / sampleRate);
    else
        return -1;
}

string IQTrace::ToString()
{
    ostringstream stringStream;
    stringStream << blockLength << ";"
		 << sampleRate  << ";"
		 << bandwidth << ";"
		 << centerFreq << ";";

    int halfSize = buffer->size() / 2;

    for(int i = 0; i < halfSize; i++)
    {
        stringStream << buffer->at(i) << ";"
    		     << buffer->at(i + halfSize) << ";";
    }

    return stringStream.str();
}


// getters and setters
vector<float>::iterator IQTrace::getBufferStartIter()
{
    return buffer->begin();
}

int IQTrace::getDoubleBlockLen()
{
    return buffer->size();
}

int IQTrace::getSampleRate()
{
    return sampleRate;
}

double IQTrace::getBandwidth()
{
    return bandwidth;
}

double IQTrace::getCenterFreq()
{
    return centerFreq;
}

/*
void IQTrace::ResizeBuffer(int size)
{

    this->buffer.resize(size);
}

void IQTrace::MakeTimeSliceTrace(IQTrace& iqTrace, int startInd, int stopInd)
{
    iqTrace.resize(abs(stopInd - startInd));
    this->sampleRate = iqTrace.sampleRate;
    this->bandwidth = iqTrace.bandwidth;
    this->centerFreq = centerFreq;
}
*/


/*
void IQTrace::DetectCamera()
{
    // IQ vector is "buffer", don't forget it
    vector<double> bigI((buffer->size() / 2));
    vector<double> bigQ((buffer->size() / 2));
    for (long unsigned int i = 0; i < buffer->size(); i+=2)
    {
	bigI[i/2] = buffer->at(i);
	bigQ[i/2] = buffer->at(i+1);
    }

    // spectrogam
    vector<vector<double>> spectrogram;
    long unsigned int halfInterval = 1024;
    long unsigned int fullInterval = halfInterval * 2;

    // length of the interval is 2048 and we take them with 50% overlapping
    long unsigned int intervalsQuantity = ((buffer->size() / 2) / fullInterval) * 2 - 1; // throw everything that doesn't fit

    if (((buffer->size() / 2) / fullInterval) == 0)
	throw runtime_error("IQ block of incorrect length is given!");


    //double fftSumTimr = 0;
    //clock_t FFTbegin;
    //clock_t FFTend;


    //clock_t begin = clock();
        // iterators for large IQ block
    vector<double>::iterator startIterI = bigI.begin();
    vector<double>::iterator endIterI = startIterI + fullInterval;

    vector<double>::iterator startIterQ = bigQ.begin();
    vector<double>::iterator endIterQ = startIterQ + fullInterval;

    vector<double> I;
    vector<double> Q;
    vector<double> fftRe;
    vector<double> fftIm;
    vector<double> fftAbs(fullInterval);


    for (long unsigned int i = 0 ; i < intervalsQuantity ; i++)
    {
	I = vector<double>(startIterI, endIterI);
	Q = vector<double>(startIterQ, endIterQ);

	//FFTbegin = clock();
	MathModule::HammingWindow(I);
	MathModule::HammingWindow(Q);
	MathModule::FFT(I, Q, fftRe, fftIm);
	//FFTend = clock();
	//fftSumTimr += (double(FFTend - FFTbegin) / CLOCKS_PER_SEC);

	// calculating absolute value and centering the domain
	// www.fftw.org/faq/section3
	// question 3.11
	for (long unsigned int n = 0 ; n < fftRe.size() ; n++)
	{
	    // same result as PrepareCenteredFFTData();
	    // but I prefer this way
	    long unsigned int index;
	    if (n >= fftRe.size()/2)
		index = n - fftRe.size()/2;
	    else
		index = n + fftRe.size()/2;
	
	    complex<double> num(fftRe[n], fftIm[n]);
	    fftAbs[index] = abs(num);
	    //fftAbs[index] = sqrt(pow(fftRe[n], 2) + pow(fftIm[n], 2));
	}

	spectrogram.push_back(fftAbs);

	startIterI += halfInterval;
	endIterI += halfInterval;
	startIterQ += halfInterval;
	endIterQ += halfInterval;
    }

    //clock_t end = clock();
    //cout << "FFT overall time: " << double(end - begin) / CLOCKS_PER_SEC << endl;
    //cout << "only FFT time: " << fftSumTimr << endl;

    // debug // works
    //for (long unsigned int i = 0 ; i < spectrogram.size() ; i++)
    //	MathModule::VecToOutput(spectrogram[i]);

    // Next - max hold 750 microseconds
    // 0.025 usec
    double binTime = 0.000000025; // usec, see Signal Hound documentation (bbFetchRaw, bbGetIQ)
    double maxHoldTime = 0.00075;
    long unsigned int frameLength = static_cast<long unsigned int>(maxHoldTime / (binTime * fullInterval)); // 750 / 1 spectrogram segment
    long unsigned int frames = spectrogram.size() / frameLength;

    //cout << "Frame length: " << frameLength << endl;
    //cout << "Frames: " << frames << endl;

    vector<double> frame(fullInterval);
    vector<vector<double>> periodogram;

    for (long unsigned int i = 0; i < frames; i++)
    {
	frame = spectrogram[i * frameLength];
	for (long unsigned int n = (i * frameLength + 1) ; n < (i * frameLength + frameLength) ; n++)
	{
	    for (long unsigned int k = 0; k < spectrogram[n].size(); k++)
	    {
		if (frame[k] < spectrogram[n][k])
		    frame[k] = spectrogram[n][k];
	    }
	}

	periodogram.push_back(frame);
    }

    // debug //works
    //for (long unsigned int i = 0 ; i < periodogram.size() ; i++)
    //	MathModule::VecToOutput(periodogram[i]);

    // periodogram [timeIndex][FreqIndex]
    //looking for time and freq with max power
    vector<double> timeMaxHold(periodogram.size());

    // preparing times MaxHold
    for (long unsigned int i = 0; i < periodogram.size(); i++)
	timeMaxHold[i] = periodogram[i][0];

    for (long unsigned int n = 0; n < periodogram[0].size(); n++) // full interval
    {
	for (long unsigned int i = 0; i < periodogram.size(); i++)
	{
	    if (timeMaxHold[i] < periodogram[i][n])
		timeMaxHold[i] = periodogram[i][n];
	}
    }


    // timeMaxHold is ready. Now looking for lower spectrum half
    vector<double> powerMaxHold(periodogram[0].size());

    // preparing powers MaxHold
    for (long unsigned int i = 0; i < periodogram[0].size(); i++)
	powerMaxHold[i] = periodogram[0][i];

    for (long unsigned int i = 0; i < periodogram.size(); i++) // full interval
    {
	for (long unsigned int n = 0; n < periodogram[i].size(); n++)
	{
	    if (powerMaxHold[n] < periodogram[i][n])
		powerMaxHold[n] = periodogram[i][n];
	}
    }

    //

    // debug // works
    //MathModule::VecToOutput(powerMaxHold);

    // cutting off zeros
    double freqBinSize = sampleRate / fullInterval;
    long unsigned int distanceToCut = static_cast<long unsigned int>(((double(40 - 27) * 1e6) / 2) / freqBinSize); // TODO - magic numbers
    // cutting off zeros and calculating mean
    double MeanMaxHold = accumulate(powerMaxHold.begin() + distanceToCut, powerMaxHold.end() - distanceToCut, 0.0) / (powerMaxHold.size() - (distanceToCut * 2));
    long unsigned int passIndexForward = 0;
    long unsigned int passIndexBackward = 0;

    for(long unsigned int i = 0; MeanMaxHold > powerMaxHold[i]; i++)
	passIndexForward = i;

    for(long unsigned int i = (powerMaxHold.size() - 1); MeanMaxHold > powerMaxHold[i]; i--)
	passIndexBackward = i;

    long unsigned int middleIndex = passIndexForward + (passIndexBackward - passIndexForward) / 2;
    //double firstFreq = (passIndexForward - timeMaxHold.size()/2) * freqBinSize;
    //double secondFreq = (passIndexBackward - timeMaxHold.size()/2) * freqBinSize;
    double band = (passIndexBackward - passIndexForward) * freqBinSize;

    cout << "bandpass " << band << endl;

    if (band < 6.0e6 && band > 5.0e6)
	cout << "probably, camera!" << endl;

    long unsigned int timeIndex = distance(timeMaxHold.begin(), max_element(timeMaxHold.begin(), timeMaxHold.end())); // may be bottleneck if container is not vector
    // looking for max freq
    //double freqBinSize = sampleRate / fullInterval;
    long unsigned int freqIndex = distance(periodogram[timeIndex].begin(), max_element(periodogram[timeIndex].begin(), periodogram[timeIndex].begin() + middleIndex));
    //long unsigned int freqIndex = distance(periodogram[timeIndex].begin(), max_element(periodogram[timeIndex].begin(), periodogram[timeIndex].end()));
    double maxPowerFreq = (long signed int)(freqIndex - periodogram[timeIndex].size()/2) * freqBinSize; // relative freq

    cout << maxPowerFreq << endl;
    //debug // works
    //MathModule::VecToOutput(timeMaxHold);

    // shift signal freq to max power freq
    MathModule::ShiftFreq(bigI, bigQ, maxPowerFreq, binTime);
    cout << "shifted" << endl;
    // debug //works
    //MathModule::VecToOutput(bigI);
    //MathModule::VecToOutput(bigQ);

    //design filter // went to Math module, call in controller
    //filter
    clock_t begin_f = clock();
    MathModule::Filter(bigI);
    MathModule::Filter(bigQ);

    clock_t end_f = clock();
    cout << "Filtering time: " << double(end_f - begin_f) / CLOCKS_PER_SEC << endl;

    // power
    long unsigned int binsPerFrame = static_cast<long unsigned int>(maxHoldTime / binTime);
    vector<double> filteredSignalEnergy;
    double powerSum = 0;
    for (long unsigned i = 0; i < bigI.size(); i++)
    {
	powerSum += pow(bigI[i], 2) + pow(bigQ[i], 2);
	if(((i + 1) % binsPerFrame) == 0)
	{
	    filteredSignalEnergy.push_back(powerSum);
	    powerSum = 0;
	}
    }
    filteredSignalEnergy.push_back(powerSum);

    // filteredSignal output
    //MathModule::VecToOutput(filteredSignalEnergy);

    //autocorrelation
    filteredSignalEnergy = MathModule::Autocorrelation(filteredSignalEnergy);

    // autocorrelation debug
    //MathModule::VecToOutput(filteredSignalEnergy);

    // search peak distance
    double stepTime =  binsPerFrame * binTime;
    double overallTime = filteredSignalEnergy.size() * stepTime;
    double peakPeriod = 0.021; // sync-pulse period is about 21 millisecond
    long unsigned int periodsQuantity = static_cast<long unsigned int>(overallTime / peakPeriod);
    vector<int> indices;


    Util::findPeaks(filteredSignalEnergy, indices);

    for (long unsigned int i = 0; i < indices.size(); i++)
    	cout << indices[i] << endl;

    int intervals = 0;
    sort(indices.begin(), indices.end());
    for (long unsigned i = 0; i < indices.size() - 1; i++)
    {
	double distance;
	distance = (indices[i+1] - indices[i]) * stepTime;
	//cout << "Distance: " << distance << endl;
	if ((distance < 0.022) && (distance > 0.019))
	    intervals++;
    }

    //cout << "Intervals Detected: " << intervals << endl;
    if (intervals > 1)
	cout << "Camera!" << endl;
    else
	cout << "NoCam" << endl;
}
*/
