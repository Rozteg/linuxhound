#include "MathModule.hpp"
#include "FourierModule.hpp"

using namespace std;
using namespace arma;

// dBm -> mWatt
float MathModule::Linearize(float value)
{
    return pow(10, (value/10));
}

// mWatt -> dBm
float MathModule::Logarithmize(float value)
{
    return (10 * log10(value));
}

void MathModule::SavitzyGolaySmoothing(vector<float>& invector, int windowWidth, int order)
{
    vector<double> inVec = vector<double>(invector.begin(), invector.end());
    vector<double> outVec = sg_smooth(inVec, windowWidth, order);

    for (int i = 0; i < outVec.size(); i++)
        invector[i] = static_cast<float> (outVec[i]);
}

void MathModule::WaveletDenoise(vector<float>& dataVector)
{
    vector<double> dwtOutput, flag;
    vector<double> invec(dataVector.begin(), dataVector.end());
    vector<int> length;
    string wavelet = "sym8";

    dwt(invec, 5, wavelet, dwtOutput, flag, length);

    float threshold = sqrt(2 * log(invec.size()));
    for (unsigned int i = 0; i < dwtOutput.size(); i++)
    {
        float temp = abs(dwtOutput[i]);
        if (temp < threshold)
            dwtOutput[i] = 0.0;
    }

    // invec.clear(); // not really needed
    idwt(dwtOutput, flag, wavelet, invec, length);

    for (int i = 0; i < invec.size(); i++)
        dataVector[i] = static_cast<float> (invec[i]);
}

void MathModule::NormZeroToOne(vector<float>& inVec, float max, float min)
{
    if (max == 0)
	   max = *max_element(begin(inVec), end(inVec));

    if (min == 0)
	   min = *min_element(begin(inVec), end(inVec));

    float delta = max - min;

    for(int i = 0; i < (int) inVec.size(); i++)
	   inVec[i] = (inVec[i] - min) / delta;
}

void MathModule::SinWaveGen(vector<float>& sigVector, double detectionTime, double frequency, double samplingFreq, double phase)
{
    long unsigned int samples = static_cast<long unsigned int>(detectionTime * samplingFreq) + 1;
    //cout << samples << endl;
    sigVector.resize(samples * 2);

    float samplingDelta = 1 / samplingFreq;

    for (long unsigned int i = 0; i < (samples * 2) ; i+=2)
    {
	   sigVector[i] = cos(2 * datum::pi * frequency * (i/2) * samplingDelta + phase); // I component
	   sigVector[i+1] = sin(2 * datum::pi * frequency * (i/2) * samplingDelta + phase); // Q component
    }
}

void MathModule::FFT(vector<float>& iVector, vector<float>& qVector, vector<float>& outRealVector, vector<float>& outImVector)
{
    if ((iVector.size() != qVector.size()) || iVector.size() == 0)
	throw runtime_error("I and Q vectors have different dimensions or have zero length!");

    long unsigned int sizeToPrepare = iVector.size();
    fftwf_complex* in = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*sizeToPrepare);
    fftwf_complex* out = (fftwf_complex*) fftw_malloc(sizeof(fftwf_complex)*sizeToPrepare);
    fftwf_plan my_plan = fftwf_plan_dft_1d(sizeToPrepare, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    for(long unsigned int i = 0; i < iVector.size() ; i++)
    {
	   in[i][0] = iVector[i];
	   in[i][1] = qVector[i];
    }

    fftwf_execute(my_plan);

    outRealVector.resize(iVector.size());
    outImVector.resize(iVector.size());

    for (long unsigned int i = 0; i < iVector.size() ; i++)
    {
	   outRealVector[i] = out[i][0];
	   outImVector[i] = out[i][1];
    }

    fftwf_destroy_plan(my_plan);
    fftwf_free(in);
    fftwf_free(out);
}

void MathModule::HammingWindow(vector<float>& input)
{
    long unsigned int N = input.size();

    for (long unsigned i = 0; i < N ; i++)
	   input[i] *= ( 0.53836 - 0.46164 * cos((2 * datum::pi * i)/(N - 1)) );
}

void MathModule::HammingCoefs(vector<float>& coefs)
{
    long unsigned int N = coefs.size();

    for (long unsigned i = 0; i < N ; i++)
        coefs[i] = ( 0.53836 - 0.46164 * cos((2 * datum::pi * i)/(N - 1)) );
}

void MathModule::ShiftFreq(vector<float>& re, vector<float>& im, float freqCenter, float dT)
{
    for (long unsigned i = 0; i < re.size(); i++)
    {
        complex<float> num(re[i], im[i]);
        float a = abs(num);
        float phase = arg(num);
        re[i] = a * cos(phase - 2 * datum::pi * freqCenter * (dT * i));
        im[i] = a * sin(phase - 2 * datum::pi * freqCenter * (dT * i));
    }
}

/*
void MathModule::CreateFilter(int M, double NormalizedFreq)
{
    vec coef = sp::fir1(M, NormalizedFreq); // 3e6/(Fsample/2) // 194, 0.15
    MathModule::lowPassFilter.set_coeffs(coef);
    cout << "low pass filter designed" << endl;
}
*/

vector<float> MathModule::Autocorrelation(vector<float>& invec)
{
    vector<float> autocorr;

    long unsigned int lag = invec.size();
    double mean = accumulate(invec.begin(), invec.end(), 0.0)/invec.size();
    double divisor = 0;

    autocorr.reserve(lag);

    for (long unsigned int i = 0; i < invec.size(); i++)
	   divisor += pow((invec[i] - mean), 2);

    for (long unsigned int k = 0; k < lag; k++)
    {
        double dividend = 0;
    	for (long unsigned i = 0; i < (invec.size() - k); i++)
            dividend += (invec[i] - mean) * (invec[i+k] - mean);
    
	   autocorr.push_back((dividend/divisor));
    }

    return autocorr;
}

void MathModule::VecToOutput(vector<float>& outVec)
{
    for (long unsigned int i = 0; i < outVec.size(); i++)
    {
        cerr << outVec[i];
        if ((i+1) != outVec.size())
            cerr << ";";
    }

    cerr << endl;
}

void MathModule::VecToOutput(vector<double>& outVec)
{
    for (long unsigned int i = 0; i < outVec.size(); i++)
    {
        cerr << outVec[i];
        if ((i+1) != outVec.size())
            cerr << ";";
    }

    cerr << endl;
}

/* smoothing */
/* http://kris.kalish.net/2014/01/porting-matlabs-discrete-gaussian-smoothing-to-c/ */
vector<double> MathModule::SmoothVector(vector<double> vect, int windowWidth)
{
    vector<double> gaussFilter;
    vector<double> result;
    vector<double> smoothedVector;

    gaussFilter = MathModule::MakeGaussianWindow(windowWidth, 2.5);
    gaussFilter = MathModule::Normalize(gaussFilter);

    // Note the use of rounding here instead of truncating integer divsion
    int halfWidth = floor((double)windowWidth / 2.0 + 0.5);

    smoothedVector = MathModule::Convolve(vect, gaussFilter);
    result = std::vector(
        smoothedVector.begin() + halfWidth-1,
        smoothedVector.end() - halfWidth
    );

    return result;
}



vector<double> MathModule::MakeGaussianWindow(int size, double alpha)
{
    vector<double> window(size, 0.0);
    int halfWidth = size/2; // integer division (truncating)

    for( int i = 0; i < size; ++i )
    {
        // w(n) = e^( -1/2 * ( alpha * n/(n/2) )^2 )
        // for n such that -N/2 <= n <= N/2

        int n = i - halfWidth;

        window[i] = pow( datum::e, -0.5 * (
            ( alpha * n / ( (double) halfWidth ) ) *
            ( alpha * n / ( (double) halfWidth ) )
            )
        );
    }

    return window;
}

vector<double> MathModule::Convolve(vector<double> f, vector<double>& g)
{
    int halfKernelWidth = g.size() / 2; // truncating division

    // Pad f in matlab style
    f.insert(f.begin(), halfKernelWidth, 0.0);
    f.insert(f.end(), 2*halfKernelWidth, 0.0);

    //std::vector< double > result( f.size()+2*halfKernelWidth, 0.0 );
    vector<double> result( f.size(), 0.0 );


    // Set result[g.size()-1] to result[f.size()-1]
    for(int i = g.size() - 1; i < f.size(); i++)
    {
        for(int j = i, k = 0; k < g.size(); j--, k++ )
        {
            result[i] += f[j] * g[k];
        }
    }

    // Set result[0] through result[g.size()-2]
    for( int i = 0; i < g.size() - 1; ++i )
    {
        for( int j = i, k = 0; j >= 0; --j, ++k )
        {
            result[i] += f[j] * g[k];
        }
    }

    // more matlab-ism
    result.erase(result.begin(), result.begin()+3);

    return result;
}

vector<double> MathModule::Normalize(vector<double> vect)
{
    double sum;
    vector<double> result;

    sum = 0.0;
    for( unsigned int i = 0; i < vect.size(); ++i )
        sum += vect[i];

    assert( sum != 0 );

    for( unsigned int i = 0; i < vect.size(); ++i )
        result.push_back( vect[i] / sum );

    return result;
}

/* функции для float */
/* Да, я знаю, что такое темплейты,
сделайте сами, мне лень :)*/

vector<float> MathModule::SmoothVector(vector<float> vect, int windowWidth)
{
    vector<float> gaussFilter;
    vector<float> result;
    vector<float> smoothedVector;

    gaussFilter = MathModule::MakeGaussianWindow(windowWidth, (float)2.5);
    gaussFilter = MathModule::Normalize(gaussFilter);

    // Note the use of rounding here instead of truncating integer divsion
    int halfWidth = floor((float)windowWidth / 2.0 + 0.5);

    smoothedVector = MathModule::Convolve(vect, gaussFilter);
    result = std::vector(
        smoothedVector.begin() + halfWidth-1,
        smoothedVector.end() - halfWidth
    );

    return result;
}



vector<float> MathModule::MakeGaussianWindow(int size, float alpha)
{
    vector<float> window(size, 0.0);
    int halfWidth = size/2; // integer division (truncating)

    for( int i = 0; i < size; ++i )
    {
        // w(n) = e^( -1/2 * ( alpha * n/(n/2) )^2 )
        // for n such that -N/2 <= n <= N/2

        int n = i - halfWidth;

        window[i] = pow( datum::e, -0.5 * (
            ( alpha * n / ( (float) halfWidth ) ) *
            ( alpha * n / ( (float) halfWidth ) )
            )
        );
    }

    return window;
}

vector<float> MathModule::Convolve(vector<float> f, vector<float>& g)
{
    int halfKernelWidth = g.size() / 2; // truncating division

    // Pad f in matlab style
    f.insert(f.begin(), halfKernelWidth, 0.0);
    f.insert(f.end(), 2*halfKernelWidth, 0.0);

    //std::vector< float > result( f.size()+2*halfKernelWidth, 0.0 );
    vector<float> result( f.size(), 0.0 );


    // Set result[g.size()-1] to result[f.size()-1]
    for(int i = g.size() - 1; i < f.size(); i++)
    {
        for(int j = i, k = 0; k < g.size(); j--, k++ )
        {
            result[i] += f[j] * g[k];
        }
    }

    // Set result[0] through result[g.size()-2]
    for( int i = 0; i < g.size() - 1; ++i )
    {
        for( int j = i, k = 0; j >= 0; --j, ++k )
        {
            result[i] += f[j] * g[k];
        }
    }

    // more matlab-ism
    result.erase(result.begin(), result.begin()+3);

    return result;
}

vector<float> MathModule::Normalize(vector<float> vect)
{
    float sum;
    vector<float> result;

    sum = 0.0;
    for( unsigned int i = 0; i < vect.size(); ++i )
        sum += vect[i];

    assert( sum != 0 );

    for( unsigned int i = 0; i < vect.size(); ++i )
        result.push_back( vect[i] / sum );

    return result;
}
/* end smoothing */

float MathModule::CalculateFloatMode(vector<float>& invec, float epsilon)
{
    vector<int> frequencyOfAppearance;
    vector<float>::iterator maxIter = max_element(begin(invec), end(invec));
    int numOfRanges = (int)ceil((*maxIter) / epsilon);
    frequencyOfAppearance.resize(numOfRanges, 0);

    for (int i = 0; i < invec.size(); i++)
    {
        int index = (int)floor(invec[i] / epsilon);
        frequencyOfAppearance[index] += 1;
    }

    float mode = (*max_element(begin(frequencyOfAppearance), end(frequencyOfAppearance))) * epsilon;
    return mode;
}

void MathModule::MovingMax(vector<float>& invec, int windowWidth)
{
    if(windowWidth > invec.size() || windowWidth == 0)
        return;

    vector<float> movmax(invec.size());

    for (int i = 0; i <= (invec.size() - windowWidth); i++)
    {
        float initialValue = invec[i];
        for (int k = 0; k < windowWidth; k++)
        {
            if(initialValue < invec[i+k])
                initialValue = invec[i+k];
        }
        for (int k = 0; k < windowWidth; k++)
        {
            //invec[i+k] = initialValue;
            movmax[i+k] = initialValue;
        }
    }

    copy (movmax.begin(), movmax.end(), invec.begin());
}

void MathModule::MovingMean(vector<float>& invec, int windowWidth)
{
    if(windowWidth > invec.size() || windowWidth == 0)
        return;

    vector<float> movmean(invec);

    for (int i = 0; i <= (invec.size() - windowWidth); i++)
    {
        float cumulative = 0;
        for (int k = 0; k < windowWidth; k++)
        {
            cumulative += invec[i+k];
        }
        movmean[i + windowWidth - 1] = cumulative / windowWidth;
    }

    copy (movmean.begin(), movmean.end(), invec.begin());
}