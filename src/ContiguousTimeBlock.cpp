#include "ContiguousTimeBlock.hpp"

using namespace std;
using namespace arma;

ContiguousTimeBlock::ContiguousTimeBlock()
{

}

void ContiguousTimeBlock::PushToBlock(ContiguousFreqBlock& cfb)
{
	freqblockVect.push_back(cfb);
}


void ContiguousTimeBlock::ToSpectrum()
{
	for (int i = 0; i < freqblockVect.size(); i++)
	{
		freqblockVect[i].ToSpectrum();
	}
}

vector<float> ContiguousTimeBlock::FreqProjection()
{
	//Max Hold of all freq blocks
	vector<float> freqProjection;
	freqProjection = freqblockVect[0].FreqProjection();


	for (int i = 1; i < freqblockVect.size(); i++)
	{
		vector<float> singleFreqProjection;
		singleFreqProjection = freqblockVect[i].FreqProjection();
		for (int n = 0; n < singleFreqProjection.size(); n++)
		{
			if (freqProjection[n] < singleFreqProjection[n])
				freqProjection[n] = singleFreqProjection[n];
		}
	}

	return freqProjection;
}

vector<float> ContiguousTimeBlock::TimeProjection()
{
	int len = 0;
	for (int i = 0; i < freqblockVect.size(); i++)
		len += freqblockVect[i].TimeSize();

	vector<float> timeProjection(len);
	vector<float>::iterator iter = timeProjection.begin();

	vector<float> singleTimeProjection;
	for (int i = 0; i < freqblockVect.size(); i++)
	{
		singleTimeProjection = freqblockVect[i].TimeProjection();
		copy (singleTimeProjection.begin(), singleTimeProjection.end(), iter);
		iter += singleTimeProjection.size();
	}

	return timeProjection;
}

void ContiguousTimeBlock::ToOutput()
{
	for (int i = 0; i < freqblockVect.size(); i++)
		freqblockVect[i].ToOutput();
}

vector<SpectrumDataBlock> ContiguousTimeBlock::GetThresholdSlicedDatablocks(float threshold)
{
	vector<SpectrumDataBlock> slicedDataBlocks;
	vector<SpectrumDataBlock> intermediateSlicedDataBlocks;
	for (int i = 0; i < freqblockVect.size(); i++)
	{
		intermediateSlicedDataBlocks = freqblockVect[i].GetThresholdSlicedDatablocks(threshold);
		for (int i = 0; i < intermediateSlicedDataBlocks.size(); i++)
		{
			slicedDataBlocks.push_back(intermediateSlicedDataBlocks[i]);
		}
	}

	return slicedDataBlocks;
}


