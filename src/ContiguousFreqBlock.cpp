#include "ContiguousFreqBlock.hpp"

using namespace std;
using namespace arma;

ContiguousFreqBlock::ContiguousFreqBlock()
{
	
}

void ContiguousFreqBlock::PushToBlock(IQPeriodogramBlock& iqpb)
{
	freqblock.push_back(iqpb);
}

void ContiguousFreqBlock::ToSpectrum()
{
	int size = this->freqblock.size();
	for (int i = 0; i < size; i++)
	{
		(freqblock.at(i)).IQToSpectrum();
	}
}

int ContiguousFreqBlock::TimeSize()
{
	int size = 0;
	for (int i = 0; i < freqblock.size(); i++)
	{
		size += freqblock[i].GetTimeSize();
	}

	return size;
}

vector<float> ContiguousFreqBlock::TimeProjection()
{
	vector<float> timeProjection;
	for (int i = 0; i < freqblock.size(); i++)
	{
		vector<float> v = freqblock[i].TimeProjection();
		timeProjection.insert(timeProjection.end(), v.begin(), v.end());
	}
	return timeProjection;
}

vector<float> ContiguousFreqBlock::FreqProjection()
{
	vector<float> freqProjection;
	int binsOverlap = freqblock[0].GetFreqSize() * (1 - overlapping);
	int freqProjSize = freqblock[0].GetFreqSize() + ((freqblock.size() - 1) * binsOverlap);
	freqProjection.resize(freqProjSize, 0);
	int iter = 0;

	for (int i = 0; i < freqblock.size(); i++)
	{
		vector<float> singleFreqProj;
		singleFreqProj = freqblock[i].FreqProjection();

		for (int n = 0; n < singleFreqProj.size(); n++)
		{
			if (freqProjection[iter] < singleFreqProj[n])
				freqProjection[iter] = singleFreqProj[n];
			iter++;
		}
		iter -= singleFreqProj.size() - binsOverlap;
	}

	return freqProjection;
}



void ContiguousFreqBlock::ToOutput()
{
	for (int i = 0; i < freqblock.size(); i++)
		freqblock[i].ToOutput();
}

vector<SpectrumDataBlock> ContiguousFreqBlock::GetThresholdSlicedDatablocks(float threshold)
{
	vector<SpectrumDataBlock> slicedDataBlocks;
	vector<SpectrumDataBlock> intermediateSlicedDataBlocks;
	for (int i = 0; i < freqblock.size(); i++)
	{
		intermediateSlicedDataBlocks = freqblock[i].GetThresholdSlicedDatablocks(threshold);
		for (int i = 0; i < intermediateSlicedDataBlocks.size(); i++)
		{
			slicedDataBlocks.push_back(intermediateSlicedDataBlocks[i]);
		}
	}

	return slicedDataBlocks;
}
