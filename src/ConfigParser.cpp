#include "ConfigParser.hpp"

using namespace std;
using namespace tinyxml2;

vector<DeviceSettings> ConfigParser::settingsVec; // в этом векторе хранятся вне настройки спектроанализатора
double ConfigParser::envelopeOffset = 0; // Смещение огибающей фона вверх иди вниз в децибелах

void ConfigParser::ParseDeviceSettings(string file)
{
	XMLDocument doc;

	if(XML_SUCCESS != doc.LoadFile(file.c_str()))
		throw runtime_error("Cannot load config!");

	XMLElement *levelElement = doc.FirstChildElement("config")->FirstChildElement("deviceSettings");
	for (XMLElement* settings = levelElement->FirstChildElement(); settings != NULL; settings = settings->NextSiblingElement())
	{
		ConfigParser::settingsVec.push_back(ConfigParser::SettingsFromElement(settings));
	}
}

DeviceSettings ConfigParser::SettingsFromElement(XMLElement* settings)
{
	int detector;
	int scale;
	double center;
	double span;
	double ref;
	int atten;
	int gain;
	double resolutionBandwidth;
	double videoBandwidth;
	double sweepTime;
	int rbwShape;
	int rejection;
	int procUnits;

	// Функция QueryAtribute возвращает код ошибки, просто IntAttribute возвращает значение по умолчанию, если не получилось прочитать атрибут
	settings->QueryIntAttribute("detector", &detector);
	settings->QueryIntAttribute("scale", &scale);
	settings->QueryDoubleAttribute("center", &center);
	settings->QueryDoubleAttribute("span", &span);
	settings->QueryDoubleAttribute("ref", &ref);
	settings->QueryIntAttribute("atten", &atten);
	settings->QueryIntAttribute("gain", &gain);
	settings->QueryDoubleAttribute("resolutionBandwith", &resolutionBandwidth);
	settings->QueryDoubleAttribute("videoBandwith", &videoBandwidth);
	settings->QueryDoubleAttribute("sweepTime", &sweepTime);
	settings->QueryIntAttribute("rbwShape", &rbwShape);
	settings->QueryIntAttribute("rejection", &rejection);
	settings->QueryIntAttribute("procUnits", &procUnits);

	DeviceSettings devSettings;
	devSettings.setDetector(static_cast<bbDetector>(detector));
	devSettings.setScale(static_cast<bbScale>(scale));
	devSettings.setCenter(center);
	devSettings.setSpan(span);
	devSettings.setRef(ref);
	devSettings.setAtten(static_cast<bbAtten>(atten));
	devSettings.setGain(static_cast<bbGain>(gain));
	devSettings.setRBW(resolutionBandwidth);
	devSettings.setVBW(videoBandwidth);
	devSettings.setSweepTime(sweepTime);
	devSettings.setRBWShape(static_cast<bbRBWShape>(rbwShape));
	devSettings.setRejection(static_cast<bbReject>(rejection));
	devSettings.setProcUnits(static_cast<bbProcUnits>(procUnits));

	return devSettings;
}

void ConfigParser::DisplaySettings()
{
	for (int i = 0; i < (int)ConfigParser::settingsVec.size(); i++)
        cout << ConfigParser::settingsVec[i].ToString() << "\n";
}