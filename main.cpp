#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include "fftw3.h"

#include "AntennaDiagram.hpp"
#include "ConfigParser.hpp"
#include "Controller.hpp"
#include "TCPServer.h"
#include "UDPServer.h"
#include "DetectionModule.hpp"

using namespace std;

int main(int argc, char **argv)
{

    if (argc < 2)
        ConfigParser::ParseDeviceSettings("Config.xml");
    else
    {
        string config(argv[1]);
        ConfigParser::ParseDeviceSettings(config);
    }
    //SignalHound::deviceSettings = ConfigParser::settingsVec[0]; // здесь сохраняем первй попавшиеся настройки

    ConfigParser::DisplaySettings(); 

    TCPServer::Setup(5705);
    UDPServer::Setup("192.168.0.10", 11001); // можно выпилить, нафиг не нужно, делали это для испытаний - сопряжения всех устройств в одну сеть
    Controller::CheckInitDevice();
    Controller::CheckInitAntennaSwitch();
    DetectionModule::InitDetector();

    // сканируем первоначальную радиолокационную обстановку
    // можем использовать CircleSweepMultipleDetect только если используем эту функцию
    Controller::GetInitialCircleSweeps();

    while(true)
    {
        for (int i = 0; i < (int)ConfigParser::settingsVec.size(); i++)
        {
            SignalHound::ConfigureDevice(ConfigParser::settingsVec[i]);
            SignalHound::InitDeviceSweep();
        
            // вот эта функция делает круговые свипы и сравнивает их с первоначальной обстановкой
            // мы передаём в неё индекс настроек спектроанализатора, чтобы она знала, с каким эталонным свипом сравнивать снятый свип
            Controller::CircleSweepMultipleDetect(i);

            //double angle = Controller::Scan(); // Эта функция определяет, является ли сигнал ППРЧ, или нет
            //double angle = Controller::CircleSweep(1); // эта делает простой круговой свип, его можно вывести на печать

            //if (angle > 0)
            //{
            //    UDPServer::Send("Ra: " + to_string((int)angle));
            //    cout << "Direction: " << angle << endl;
            //}

        
            //cout << angle << " ± " << 45.0 / 8 << "\n";
            //TCPServer::Send(to_string(angle) + "\r\n");
            //TCPServer::Send("\r\n");
        }

        //cout << "Exited normally" << endl;
        //exit(0);
    }

    TCPServer::Detach();
    UDPServer::Close();
    SignalHound::CloseDevice();
    AntennaSwitch::CloseDevice();

    return 0;
}

