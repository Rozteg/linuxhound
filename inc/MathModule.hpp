#ifndef _MATH_MODULE_H
#define _MATH_MODULE_H

#include <cmath>
#include <cassert>
#include <vector>
#include <string>
#include <complex>
#include <armadillo>
#include "wavelet2s.h"
#include "SGSmooth.hpp"

namespace MathModule
{
	void WaveletDenoise(std::vector<float>& dataVector);
	void SavitzyGolaySmoothing(std::vector<float>& invector, int windowWidth, int order);
	void NormZeroToOne(std::vector<float>& inVec, float max = 0, float min = 0);
	void SinWaveGen(std::vector<float>& sigVector, double detectionTime, double frequency, double samplingFreq, double phase = 0);
	void FFT(std::vector<float>& iVector, std::vector<float>& qVector, std::vector<float>& outRealVector, std::vector<float>& outImVector);
	void HammingCoefs(std::vector<float>& coefs);
	void HammingWindow(std::vector<float>& input);
	void ShiftFreq(std::vector<float>& re, std::vector<float>& im, float freqCenter, float dT);
	float Linearize(float value);
	float Logarithmize(float value);
	void MovingMax(std::vector<float>& invec, int windowWidth);
	void MovingMean(std::vector<float>& invec, int windowWidth);

	void VecToOutput(std::vector<float>& outVec);
	void VecToOutput(std::vector<double>& outVec);
	
	void CreateFilters();
	void Filter(std::vector<double>& signal);
	
	std::vector<float> Autocorrelation(std::vector<float>& invec);

	/* smoothing */
	std::vector<double> SmoothVector(std::vector<double> vect, int windowWidth);
	std::vector<double> MakeGaussianWindow(int size, double alpha);
	std::vector<double> Convolve(std::vector<double> f, std::vector<double>& g);
	std::vector<double> Normalize(std::vector<double> vect);

	std::vector<float> SmoothVector(std::vector<float> vect, int windowWidth);
	std::vector<float> MakeGaussianWindow(int size, float alpha);
	std::vector<float> Convolve(std::vector<float> f, std::vector<float>& g);
	std::vector<float> Normalize(std::vector<float> vect);
	/* end smoothing*/

	float CalculateFloatMode(std::vector<float>& invec, float epsilon);
}

#endif // _MATH_MODULE_H 