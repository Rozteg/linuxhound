#ifndef _CONTIGUOUS_TIME_BLOCK_H 
#define _CONTIGUOUS_TIME_BLOCK_H 

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib>
#include <memory>
#include <armadillo>
#include "MathModule.hpp"
#include "ContiguousFreqBlock.hpp"
#include "SpectrumDataBlock.hpp"
#include "IQPeriodogramBlock.hpp"
#include "Util.hpp"

class ContiguousTimeBlock {

public:
    std::vector<ContiguousFreqBlock> freqblockVect;

    ContiguousTimeBlock();
    void PushToBlock(ContiguousFreqBlock& cfb);
    std::vector<float> FreqProjection();
    std::vector<float> TimeProjection();
    void ToSpectrum();
    void ToOutput();
    std::vector<SpectrumDataBlock> GetThresholdSlicedDatablocks(float threshold);
};

#endif // _CONTIGUOUS_TIME_BLOCK_H 