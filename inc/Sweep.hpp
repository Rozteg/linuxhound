#ifndef _SWEEP_H
#define _SWEEP_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib>
#include <memory>
#include <armadillo>
#include "MathModule.hpp"


class Sweep {

private:
    unsigned int traceLen;
    double binSize;
    double startFreq;
    bool linearized;

public:
    std::shared_ptr<std::vector<float>> sweepMax;
    std::shared_ptr<std::vector<float>> sweepMin;

    Sweep();
    Sweep(unsigned int traceLen, double binSize, double startFreq);
    float GetFreq(int i);
    void Merge(Sweep& sweep);
    void Smoothen();
    std::string ToString();
    void Linearize();
    void Logarithmize();
    float Integrate();
    float Integrate(int start, int end);
    Sweep SliceByIndexes(int startInd, int endInd);
    void MoveUpDown(float dBm);

    float GetMax();
    float GetMin();

    int GetMaxIndex();
    float GetMaxFreq();

    // getters and setters
    unsigned int getTraceLen();
    double getBinSize();
    double getStartFreq();
};

#endif // _SWEEP_H 