#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>


namespace UDPServer
{
	extern int sockfd;
	extern struct sockaddr_in servaddr;
	extern bool isSet;

	int Setup(std::string addr, int port);
	int Send(std::string msg);
	void Close();
};

#endif
