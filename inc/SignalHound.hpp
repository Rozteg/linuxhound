#ifndef _SIGNAL_HOUND_H
#define _SIGNAL_HOUND_H

#include <iostream>
#include <string>
#include <vector>
#include <exception>

#include "bb_api.h"
#include "Sweep.hpp"
#include "DeviceSettings.hpp"


namespace SignalHound
{
    extern int id;
    extern bbStatus status;
    extern DeviceSettings deviceSettings;
    extern Sweep lastSweep;

    // Sweep characteristics
    extern unsigned int traceLen;
    extern double binSize;
    extern double startFreq;

    // IQ charcteristics
    extern double bandwidth;
    extern int sampleRate;
    extern int blockLength;

    bbStatus OpenDevice();
    bbStatus ConfigureDevice(DeviceSettings &settings);
    bbStatus ConfigureDeviceIQ(DeviceSettings& settings);
    bbStatus ConfigureDevice();
    bbStatus InitDeviceSweep();
    bbStatus TakeSweep();
    bbStatus FetchRaw(float* buffer);
    bbStatus CloseDevice();
};

#endif // _SIGNAL_HOUND_H 