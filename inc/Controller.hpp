#ifndef _CONTROLLER_H
#define _CONTROLLER_H

#include "ConfigParser.hpp"
#include "SignalHound.hpp"
#include "AntennaSwitch.hpp"
#include "SweepCircle.hpp"
#include "IQTrace.hpp"
#include "MathModule.hpp"
#include "DetectionModule.hpp"
#include "SpectrumDataBlock.hpp"
#include "IQPeriodogramBlock.hpp"
#include "ContiguousFreqBlock.hpp"
#include "ContiguousTimeBlock.hpp"
#include "Util.hpp"
#include <unistd.h>


namespace Controller
{
	extern vector<SweepCircle> initialSweepCircles; // здесь мы сохраняем круговые свипы первичной радиоэлектронной обстановки

	void CheckInitDevice();
	void CheckInitDeviceIQ();
	void CheckInitAntennaSwitch();
	void Sweep();
	double CircleSweep(unsigned int circles = 1);
	double CircleSweep(unsigned int circles, unsigned int);
	double Scan();
	void IQTraceScan(int antennaNum);
	void AnalyseTimeBlock(ContiguousTimeBlock& timeblock); // Перенести в DetectionModule
	void ProcessSlicedSignal(SpectrumDataBlock dataBlock); // Перенести в DetectionModule
	void GetInitialCircleSweeps();
	void CircleSweepMultipleDetect(int indexOfSettings);
}

#endif // _CONTROLLER_H