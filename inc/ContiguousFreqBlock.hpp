#ifndef _CONTIGUOUS_FREQ_BLOCK_H 
#define _CONTIGUOUS_FREQ_BLOCK_H 

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib>
#include <memory>
#include <armadillo>
#include "MathModule.hpp"
#include "SpectrumDataBlock.hpp"
#include "IQPeriodogramBlock.hpp"
#include "Util.hpp"

class ContiguousFreqBlock {

public:
	float overlapping;
    std::vector<IQPeriodogramBlock> freqblock;

    ContiguousFreqBlock();
    void PushToBlock(IQPeriodogramBlock& iqpb);
    void ToSpectrum();
    int TimeSize();

    std::vector<float> TimeProjection();
    std::vector<float> FreqProjection();
    void ToOutput();
    std::vector<SpectrumDataBlock> GetThresholdSlicedDatablocks(float threshold);
};

#endif // _CONTIGUOUS_FREQ_BLOCK_H 