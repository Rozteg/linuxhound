#ifndef _DETECTION_MODULE_H
#define _DETECTION_MODULE_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib>
#include <memory>
#include <armadillo>
#include "MathModule.hpp"
#include "SweepCircle.hpp"
#include "IQTrace.hpp"
#include "SpectrumDataBlock.hpp"
#include "Util.hpp"
#include "bb_api.h"

#define FFT_WINDOW_WIDTH_HALF_INTERVAL 1024

namespace DetectionModule
{
	void CreateFilters();
	void CreateAdiagram(double step, double sigma, double offset);
	void Filter(vector<float>& signal);
	void InitDetector();
	int SimpleDetectCamera(IQTrace& iqtrace);
    int DetectCamera(IQTrace& iqtrace);
    float CalculateDirection(SweepCircle& sweepCircle);
    float CalculateDirectionStatistically(SweepCircle& sweepCircle);
	void MultipleDetect(SweepCircle& scannedCircle, SweepCircle& initialBackgroundCircle, vector<SweepCircle>& slicedCircles);
    void FillSpectrumFromIQ(IQTrace& iqtrace, SpectrumDataBlock& spectrumBlock);
};

#endif // DETECTION_MODULE_H 