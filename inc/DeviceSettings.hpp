#ifndef _DEVICE_SETTINGS_H
#define _DEVICE_SETTINGS_H

#include <string>
#include <sstream>
#include "bb_api.h"


class DeviceSettings
{
private:
    bbDetector detector;
    bbScale scale;
    // center span
    double center;
    double span;

    // level
    double ref;
    bbAtten atten;

    // gain
    bbGain gain;

    // sweep coupling
    double resolutionBandwidth;
    double videoBandwidth;
    double sweepTime;
    bbRBWShape rbwShape;
    bbReject rejection;

    // proc units
    bbProcUnits procUnits;

public:
    DeviceSettings();

    std::string ToString();

    // getters and setters
    bbDetector getDetector();
    void setDetector(bbDetector detector);
    ////

    bbScale getScale();
    void setScale(bbScale scale);
    ////

    double getCenter();
    void setCenter(double center);
    ////

    double getSpan();
    void setSpan(double span);
    ////

    double getRef();
    void setRef(double ref);
    ////

    double getAtten();
    void setAtten(bbAtten atten);
    ////

    int getGain();
    void setGain(bbGain gain);
    ////

    double getRBW();
    void setRBW(double resolutionBandwidth);
    ////

    double getVBW();
    void setVBW(double videoBandwidth);
    ////

    double getSweepTime();
    void setSweepTime(double sweepTime);
    ////

    bbRBWShape getRBWShape();
    void setRBWShape(bbRBWShape rbwShape);
    ////

    bbReject getRejection();
    void setRejection(bbReject rejection);
    ////

    bbProcUnits getProcUnits();
    void setProcUnits(bbProcUnits procUnits);
    ////
};

#endif // _DEVICE_SETTINGS_H 