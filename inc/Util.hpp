// Util.h
#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

#define EPS 2.2204e-16
namespace Util
{
    void findPeaks(std::vector<float> x0, std::vector<int>& peakInds);
    void diff(std::vector<float> in, std::vector<float>& out);
    void vectorProduct(std::vector<float> a, std::vector<float> b, std::vector<float>& out);
    void findIndicesLessThan(std::vector<float> in, float threshold, std::vector<int>& indices);
    void selectElements(std::vector<float> in, std::vector<int> indices, std::vector<float>& out);
    void selectElements(std::vector<int> in, std::vector<int> indices, std::vector<int>& out);
    void signVector(std::vector<float> in, std::vector<int>& out);
};

#endif
