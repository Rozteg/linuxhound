#ifndef _SWEEP_CIRCLE_H
#define _SWEEP_CIRCLE_H

#define AMPLITUDE_THRESHOLD -75

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <sstream>

#include "Sweep.hpp"
#include "AntennaDiagram.hpp"
#include "AntennaSwitch.hpp"
#include "MathModule.hpp"

class SweepCircle
{
private:
	void Normalize();
public:
	static const int ANTENNAS_QUANTITY = ANTENNAS; // 8
	std::vector<Sweep> sweeps;

	void Add(Sweep sweep);
	void Merge(SweepCircle sweepCircle);
	void MergeAverage(vector<SweepCircle>& sweepCircleVec);
	void Smoothen();
	SweepCircle SliceByIndexes(int startInd, int endInd);
	void MoveUpDown(float dBm);
	std::string ToString();
	std::string MaxesToString();
	bool SignalExistence(float threshold);
	int MaxPowerDirection();
	float CalculateDirection();
	float CalculateDirectionGaussian(AntennaDiagram& aDiag);
	//float CalculateDirectionGaussianMedian(AntennaDiagram& aDiag);
	float CalculateDirectionGaussianAverage(AntennaDiagram& aDiag, vector<SweepCircle>& circleVector);
};

#endif // _SWEEP_CIRCLE_H
