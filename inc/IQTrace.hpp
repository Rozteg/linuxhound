#ifndef _IQ_TRACE_H
#define _IQ_TRACE_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib>
#include <memory>
#include <armadillo>
#include "MathModule.hpp"
#include "Util.hpp"

class IQTrace {

private:
    int sampleRate;
    int blockLength;
    double bandwidth;
    double centerFreq;

public:
    std::shared_ptr<std::vector<float>> buffer;

    IQTrace();
    IQTrace(IQTrace& prototype);
    IQTrace(int sampleRate, int blockLength, double bandwidth, double centerFreq);
    void Init(int sampleRate, int blockLength, double bandwidth, double centerFreq);
    double GetTime(int i);
    void DetectCamera();
    std::string ToString();

    // getters and setters
    std::vector<float>::iterator getBufferStartIter();
    int getDoubleBlockLen();
    int getSampleRate();
    double getBandwidth();
    double getCenterFreq();
};

#endif // _IQ_TRACE_H 
