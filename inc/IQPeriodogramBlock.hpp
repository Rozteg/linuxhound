#ifndef _IQ_PERIODOGRAM_BLOCK_H
#define _IQ_PERIODOGRAM_BLOCK_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib> 
#include <memory>
#include <armadillo>
#include "MathModule.hpp"
#include "IQTrace.hpp"
#include "DetectionModule.hpp"
#include "SpectrumDataBlock.hpp"

class IQPeriodogramBlock {

public:
    SpectrumDataBlock spectrumBlock;
    IQTrace iqtrace;

    IQPeriodogramBlock();
    IQPeriodogramBlock(const IQPeriodogramBlock& prototype);
    void IQToSpectrum();

    std::vector<float> FreqProjection();
    std::vector<float> TimeProjection();
    int GetTimeSize();
    int GetFreqSize();
    void ToOutput();

    std::vector<SpectrumDataBlock> GetThresholdSlicedDatablocks(float threshold);
};

#endif // _IQ_PERIODOGRAM_BLOCK_H