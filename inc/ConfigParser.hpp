#ifndef _CONFIG_PARSER_H
#define _CONFIG_PARSER_H

#include "tinyxml2.h"
#include "DeviceSettings.hpp"
#include <vector>
#include <iostream>

namespace ConfigParser
{
	extern std::vector<DeviceSettings> settingsVec;
	extern DeviceSettings initialSettings;
	extern double envelopeOffset;

	void ParseDeviceSettings(std::string file);
	DeviceSettings SettingsFromElement(tinyxml2::XMLElement* settings);
	void DisplaySettings();
}

#endif
