#ifndef _ANTENNA_DIAGRAM_H
#define _ANTENNA_DIAGRAM_H

#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <armadillo>

class AntennaDiagram {

private:
    std::vector<double> pVec;
    double delta;
    double offset;

    static inline double AntennaDiffDivSum(double a, double b);
    static inline double AntennaMaxDivMin(double a, double b);
    static std::vector<double> AntennaDiagramTable(double step, double sigma);

public:

    AntennaDiagram();
    AntennaDiagram(double sigma, double delta, double offset);
    double GetDirectionStep(double calcValue);
    double GetDirection(double a, double b);

    // getters and setters
    double getOffset();
};

#endif // _ANTENNA_DIAGRAM_H 
