#ifndef _ANTENNA_SWITCH_H
#define _ANTENNA_SWITCH_H

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "ftd2xx.h"

#define ANTENNAS 8

namespace AntennaSwitch
{
	typedef enum Antenna
	{
		EIGHT = 0,
		SEVEN,
		FOUR,
		THREE,
		SIX,
		FIVE,
		TWO,
		ONE
	} Antenna;

	extern const std::string SERIAL_NUM;
	extern const std::string DESCRIPTION;
	extern const int ANTENNAS_QUANTITY;

	extern FT_STATUS ftStatus;
	extern FT_HANDLE ftdiDevice;

	extern const Antenna initialAntenna;

	void OpenDevice();
	Antenna GetCurrentAntenna();
	void SwitchAntenna(Antenna antenna);
	Antenna GetNextAntenna(Antenna antenna);
	void NextAntenna();
	Antenna GetPrevAntenna(Antenna antenna);
	void PrevAntenna();
	void CloseDevice();
	int AntennaToNum(Antenna antenna);
	Antenna NumToAntenna(int n);
	void InitFTDIDevice();
}

#endif // _ANTENNA_SWITCH_H
