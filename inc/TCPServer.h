#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <iostream>
#include <set>
#include <mutex>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <fcntl.h>
#include <thread>


namespace TCPServer
{
	extern int sockfd;
	extern struct sockaddr_in serverAddress;
	extern fd_set readset;
	extern int reuse;
	extern struct timeval timeout;
	extern std::set<int> connected;
	extern std::mutex connectedMutex;
	extern pthread_t serverThread;

	void Setup(int port);
	void Send(std::string msg);
	void Detach();

	void* Task(void* args);
};

#endif
