#ifndef _SPECTRUM_DATA_BLOCK_H
#define _SPECTRUM_DATA_BLOCK_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <exception>
#include <cstdlib>
#include <memory>
#include <armadillo>
#include "MathModule.hpp"
#include "IQTrace.hpp"

class SpectrumDataBlock {

public:
	double centerFreq;
    double frequencyStep;
    double timeStep;
    std::shared_ptr<std::vector<std::vector<float>>> periodogram;
    std::shared_ptr<std::vector<std::vector<float>>> spectrogram;

    SpectrumDataBlock();
    SpectrumDataBlock(const SpectrumDataBlock& prototype);
    void PushToPeriodogram(std::vector<float>& v);
    void PushToSpectrogram(std::vector<float>& v);
    int GetTimeSize();
    int GetFreqSize();
    void ToOutput();
    void ToOutputSpectr();

    std::vector<float> FreqProjection();
    std::vector<float> FreqSpectrumProjection();
	std::vector<float> TimeProjection();
    std::vector<float> TimeSpectrumProjection();
    void CalculateEntropy(std::vector<float>& entropyVec);
    void GetFreqIndexSlice(vector<vector<float>>& freqSlicedSpectrogram, vector<vector<float>>& inSpectrogram, int startInd, int endInd);
    void GetTimeIndexSlice(vector<vector<float>>& timeSlicedSpectrogram, vector<vector<float>>& inSpectrogram, int startInd, int endInd);
    std::vector<SpectrumDataBlock> GetThresholdSlicedDatablocks(float threshold);
};

#endif // _SPECTRUM_DATA_BLOCK_H
