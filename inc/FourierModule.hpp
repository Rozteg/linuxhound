#ifndef _FOURIER_MODULE_H
#define _FOURIER_MODULE_H

#include <cmath>
#include <vector>
#include <string>
#include "fftw3.h"

namespace FourierModule
{
    extern long unsigned int sizeN;
    extern fftw_complex* in;
    extern fftw_complex* out;
    extern fftw_plan my_plan;

    void PrepareFourier(long unsigned int sizeToPrepare);
    void ExecuteFourier();
    void FreeFourier();
};

#endif // _FOURIER_MODULE_H 