#ifndef _DETECTION_MODULE_FILTER_H
#define _DETECTION_MODULE_FILTER_H

#include <sigpack/sigpack.h>

namespace DetectionModule
{
	extern sp::IIR_filt<float, float, float> lowPassFilter1;
	extern sp::IIR_filt<float, float, float> lowPassFilter2;
}

#endif // _DETECTION_MODULE_H 